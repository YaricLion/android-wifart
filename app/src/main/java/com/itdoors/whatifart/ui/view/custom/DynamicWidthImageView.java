package com.itdoors.whatifart.ui.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class DynamicWidthImageView extends ImageView {

    private double mWeightRatio;

    public DynamicWidthImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicWidthImageView(Context context) {
        super(context);
    }

    public void setWeightRatio(double ratio) {
        if (ratio != mWeightRatio) {
            mWeightRatio = ratio;
            requestLayout();
        }
    }

    public double getWeightRatio() {
        return mWeightRatio;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mWeightRatio > 0.0) {
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int width = (int) (height * mWeightRatio);
            setMeasuredDimension(width, height);
        }
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
