package com.itdoors.whatifart.ui.activity.painter;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.ActivityContainer;
import com.itdoors.whatifart.ui.activity.BaseActivity;
import com.itdoors.whatifart.ui.activity.ContainerActivity;
import com.itdoors.whatifart.ui.fragment.PainterImageFragment;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import dagger.ObjectGraph;

public class PainterImageActivity extends ContainerActivity<PainterImageFragment>{

    @Override protected PainterImageFragment setupFragment(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String url = bundle.getString(Intents.Painter.PAINTER_URL);
            if(url != null)
                return PainterImageFragment.newInstance(url);
            else {
                Painter painter = bundle.getParcelable(Intents.Painter.PAINTER);
                if(painter != null)
                    return PainterImageFragment.newInstance(painter);
            }
        }
        throw new IllegalStateException("Failed PainterImageActivity setup!");
    }

}
