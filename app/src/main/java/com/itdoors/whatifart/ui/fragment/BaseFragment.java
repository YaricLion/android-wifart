package com.itdoors.whatifart.ui.fragment;

import android.support.v4.app.Fragment;
import com.itdoors.whatifart.App;

public class BaseFragment extends Fragment {
    @Override public void onDestroy() {
        super.onDestroy();
        App.getRefWatcher(getActivity()).watch(this);
    }
}
