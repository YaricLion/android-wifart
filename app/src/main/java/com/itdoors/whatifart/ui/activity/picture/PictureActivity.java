package com.itdoors.whatifart.ui.activity.picture;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.google.common.base.Strings;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.OnSharePictureActivityClickedEvent;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.ActivityContainer;
import com.itdoors.whatifart.ui.activity.BaseActivity;
import com.itdoors.whatifart.ui.activity.ContainerActivity;
import com.itdoors.whatifart.ui.fragment.PictureFragment;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import dagger.ObjectGraph;
import de.greenrobot.event.EventBus;

public class PictureActivity extends ContainerActivity<PictureFragment> {

    private ObjectGraph activityGraph;

    @Inject EventBus eventBus;

    @Override protected ObjectGraph getObjectGraph(){
        ObjectGraph appGraph = Injector.obtain(getApplication());
        activityGraph = appGraph.plus(new PictureActivityModule(this));
        return activityGraph;
    }

    @Override protected PictureFragment setupFragment(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
           String uid = bundle.getString(Intents.Picture.PICTURE_ID);
           if(!Strings.isNullOrEmpty(uid)){
               return PictureFragment.newInstance(uid);
           }
        }
        //throw new IllegalStateException("Failed PictureActivity setup :  bundle == null!"); //TODO bug on ARActivity?
        return null;
    }

    @Override  public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_picture_detail_share:
                eventBus.post(new OnSharePictureActivityClickedEvent());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return activityGraph;
        }
        return super.getSystemService(name);
    }

    @Override protected void onDestroy() {
        activityGraph = null;
        super.onDestroy();
    }

}
