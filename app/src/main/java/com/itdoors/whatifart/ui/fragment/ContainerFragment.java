package com.itdoors.whatifart.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.whatifart.ui.activity.FragmentContainer;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import dagger.ObjectGraph;

public abstract class ContainerFragment<VIEW extends ViewGroup> extends BaseFragment {

    @Inject FragmentContainer fragmentContainer;

    protected abstract int getViewId();
    protected abstract void setupView(VIEW view);

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ObjectGraph objectGraph = getObjectGraph();
        objectGraph.inject(this);

        ViewGroup rootView = (ViewGroup)fragmentContainer.create(inflater, container);
        int containerId = fragmentContainer.container();

        ViewGroup layout = (ViewGroup) rootView.findViewById(containerId);
        VIEW view = (VIEW)inflater.inflate(getViewId(), layout, false);

        setupView(view);
        layout.addView(view);

        return rootView;
    }

    protected ObjectGraph getObjectGraph(){
        return Injector.obtain(getActivity().getApplicationContext());
    }

}
