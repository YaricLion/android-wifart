package com.itdoors.whatifart.ui.view;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by v014nd on 22.03.2016.
 */
public class VersionView extends LinearLayout {

    @Inject Device device;

    @Bind(R.id.view_version_number) TextView versionView;
    @BindString(R.string.version_number)  String versionStr;

    public VersionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Injector.obtain(context).inject(this);
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        versionView.setText(String.format(versionStr, device.getVersion()));
    }

}
