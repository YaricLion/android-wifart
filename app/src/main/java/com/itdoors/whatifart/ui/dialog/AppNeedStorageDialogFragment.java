package com.itdoors.whatifart.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.permission.AllowStorageEvent;
import com.itdoors.whatifart.event.permission.DenyStorageEvent;
import com.itdoors.whatifart.ui.dialog.screenshot.ScreenShotDialogFragment;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

public class AppNeedStorageDialogFragment extends DialogFragment {

        public static AppNeedStorageDialogFragment newInstance(int code){
            AppNeedStorageDialogFragment fragment = new AppNeedStorageDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("code", code);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Inject EventBus eventBus;

        @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            Injector.obtain(getActivity().getApplicationContext()).inject(this);
            return super.onCreateView(inflater, container, savedInstanceState);
        }

        @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

            int msg = getMsg(getCode());

            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.permission_storage_screenshot_rationale_dialog_title)
                    .content(msg)
                    .positiveText(R.string.permission_tutorial_rationale_allow)
                    .positiveColorRes(R.color.red_500)
                    .negativeText(R.string.permission_tutorial_rationale_deny)
                    .negativeColorRes(R.color.blue_500)
                    .build();

            View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
            View negativeAction = dialog.getActionButton(DialogAction.NEGATIVE);

            positiveAction.setOnClickListener(view -> {
                eventBus.post(new AllowStorageEvent());
                dialog.dismiss();
            });

            negativeAction.setOnClickListener(view -> {
                eventBus.post(new DenyStorageEvent());
                dialog.dismiss();
            });

            return dialog;

        }

        private static int getMsg(int code ){
            int msgRes;
            switch (code) {
                case ScreenShotDialogFragment.ACTION_SAVE:
                    msgRes = R.string.permission_storage_screenshot_rationale_save_dialog_msg;
                    break;
                case ScreenShotDialogFragment.ACTION_SHARE:
                    msgRes = R.string.permission_storage_screenshot_rationale_share_dialog_msg;
                    break;
                default:
                    throw new IllegalStateException("Failed code : " + code);
            }
            return msgRes;
        }

        public int getCode(){
            if(getArguments()!= null){
                return getArguments().getInt("code");
            }
            else throw new IllegalStateException("Failed initialization - no code!");
        }

}
