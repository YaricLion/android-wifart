package com.itdoors.whatifart.ui.view.painter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.util.Injector;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by v014nd on 13.05.2015.
 */
public class PainterImageView extends FrameLayout{

    @Bind(R.id.painter_view_scale_image)  SubsamplingScaleImageView subsamplingScaleImageView;

    @Inject Picasso picasso;
    @Inject EventBus eventBus;

    private String url;
    private Painter painter;

    private boolean loading = true;

    public PainterImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Injector.obtain(context.getApplicationContext()).inject(this);
        }
    }

    public void setup(String url){
        this.url = url;
    }
    public void setup(Painter painter){
        this.painter = painter;
    }
    private void fill(){
        if(url != null)
            loadImage(url);
        else if(painter != null)
            loadImage(painter.getImageLink());
        else
            loading = false;
    }

    private Target target = new Target() {
        @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            subsamplingScaleImageView.setImage(ImageSource.bitmap(bitmap));
        }
        @Override public void onBitmapFailed(Drawable errorDrawable) {}
        @Override public void onPrepareLoad(Drawable placeHolderDrawable) {}
    };

    private void loadImage(String url){
        picasso.load(url).placeholder(R.drawable.image_placeholder).into( target );
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        fill();
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(loading) {
            picasso.cancelRequest(target);
        }
    }

}
