package com.itdoors.whatifart.crashlytics;

import android.util.Log;

import timber.log.Timber;

public abstract class TreeWrapper extends Timber.Tree{

    public abstract void loggingError(int priority, Throwable logging, Throwable original, String message, Object... args);

    public void v(String message, Object... args) {

        try{
            super.v(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.VERBOSE, logging, null, message, args);
        }
    }

    public void v(Throwable t, String message, Object... args) {
        try{
            super.v(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.VERBOSE, logging, t, message, args);
        }
    }

    public void d(String message, Object... args) {
        try{
            super.d(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.DEBUG, logging, null, message, args);
        }
    }

    public void d(Throwable t, String message, Object... args) {
        try{
            super.d(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.DEBUG, logging, t, message, args);
        }
    }

    public void i(String message, Object... args) {
        try{
            super.i(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.INFO, logging, null, message, args);
        }
    }

    public void i(Throwable t, String message, Object... args) {
        try{
            super.i(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.INFO, logging, t, message, args);
        }
    }

    public void w(String message, Object... args) {
        try{
            super.w(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.WARN, logging, null, message, args);
        }
    }

    public void w(Throwable t, String message, Object... args) {
        try{
            super.w(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.WARN, logging, t, message, args);
        }
    }

    public void e(String message, Object... args) {
        try{
            super.e(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.ERROR, logging, null, message, args);
        }
    }

    public void e(Throwable t, String message, Object... args) {
        try{
            super.e(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.ERROR, logging, t, message, args);
        }
    }

    public void wtf(String message, Object... args) {
        try{
            super.wtf(message, args);
        }catch (RuntimeException logging){
            loggingError(Log.ASSERT, logging, null, message, args);
        }
    }

    public void wtf(Throwable t, String message, Object... args) {
        try{
            super.wtf(t, message, args);
        }catch (RuntimeException logging){
            loggingError(Log.ASSERT, logging, t, message, args);
        }
    }


}
