package com.itdoors.whatifart.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.ShowPainterImageEvent;
import com.itdoors.whatifart.event.ShowPictureImageEvent;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.picture.PictureImageActivity;
import com.itdoors.whatifart.ui.activity.painter.PainterImageActivity;
import com.itdoors.whatifart.ui.view.picture.PictureView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

public class PictureFragment extends ContainerFragment<PictureView> {

    private static final String PICTURE_ID_TAG = "PICTURE_ID_TAG";

    @Inject EventBus eventBus;

    public static PictureFragment newInstance(String pictureId){

        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putString(PICTURE_ID_TAG, pictureId);
        fragment.setArguments(args);
        return fragment;

    }

    @Override protected int getViewId() {
        return R.layout.view_picture;
    }

    @Override protected  void setupView(PictureView view){
        Bundle args = getArguments();
        if(args != null) {
            String pictureId = args.getString(PICTURE_ID_TAG);
            if(pictureId != null){
                view.setup(pictureId);
                return;
            }
        }
        throw new IllegalStateException("Failed PictureFragment setup!");
    }

    @Override public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override public void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    public void onEventMainThread(ShowPictureImageEvent event){
        Intent intent = new Intent(getActivity(), PictureImageActivity.class);
        intent.putExtra(Intents.Picture.PICTURE, event.getPicture());
        startActivity(intent);
    }

    public void onEventMainThread(ShowPainterImageEvent event){
        Intent intent = new Intent(getActivity(), PainterImageActivity.class);
        intent.putExtra(Intents.Painter.PAINTER, event.getPainter());
        startActivity(intent);
    }

}
