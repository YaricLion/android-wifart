package com.itdoors.whatifart.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.ui.view.VersionView;

public class VersionFragment extends DialogFragment {

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        VersionView versionView = (VersionView) layoutInflater.inflate(R.layout.view_version, null);
        return new MaterialDialog.Builder(getActivity())
                .customView(versionView, false)
                .build();
    }

}
