package com.itdoors.whatifart.event;

import com.itdoors.whatifart.data.model.Painter;

public final class ShowPainterImageEvent {
    private final Painter painter;
    public ShowPainterImageEvent(Painter painter) {
        this.painter = painter;
    }
    public Painter getPainter() {
        return painter;
    }
}

