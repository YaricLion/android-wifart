package com.itdoors.whatifart.ui.fragment;

import android.os.Bundle;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.ui.view.painter.PainterImageView;

public class PainterImageFragment extends ContainerFragment<PainterImageView> {

    private static final String PAINTER_TAG = "PainterImageFragment.PAINTER_TAG";
    private static final String PAINTER_URL_TAG = "PainterImageFragment.PAINTER_URL_TAG";

    public static PainterImageFragment newInstance(Painter painter){
        PainterImageFragment fragment = new PainterImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(PAINTER_TAG, painter);
        fragment.setArguments(args);
        return fragment;
    }

    public static PainterImageFragment newInstance(String url){
        PainterImageFragment fragment = new PainterImageFragment();
        Bundle args = new Bundle();
        args.putString(PAINTER_URL_TAG, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override protected int getViewId() {
        return R.layout.view_painter_image;
    }

    @Override protected void setupView(PainterImageView view){
        Bundle args = getArguments();
        if(args != null) {
            String url = args.getString(PAINTER_URL_TAG);
            if(url != null){
                view.setup(url);
                return;
            }
            Painter painter = args.getParcelable(PAINTER_TAG);
            if(painter != null ) {
                view.setup(painter);
                return;
            }
        }
        throw new IllegalStateException("Failed PainterImageFragment setup!");
    }

}
