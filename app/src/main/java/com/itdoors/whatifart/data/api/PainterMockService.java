package com.itdoors.whatifart.data.api;


import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Address;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.data.model.PainterPicturesInfo;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.data.model.TargetPainterPictureInfo;
import com.itdoors.whatifart.util.Device;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class PainterMockService implements PainterService {

    private static final String SITE = "wifart.com";

    private static final long DELAY_MILLIS = 100L;

    private static final int DEF_CAPACITY = 5;
    private static final Map<String, PainterPicturesInfo> mockResponces = Collections.synchronizedMap(new HashMap<>(DEF_CAPACITY));

    private final Context context;
    private final Gson gson;

    public PainterMockService(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
    }

    @Override public Observable<PainterPicturesInfo> getPainterInfo(String uid) {
        return  Observable.defer(() -> Observable.just(loadPainterInfo(context, gson, uid)))
                .delay(DELAY_MILLIS, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io());
    }

    private static PainterPicturesInfo loadPainterInfo(Context context, Gson gson, String uid){
        Device.throwIfOnMainThread();
        synchronized (mockResponces) {
            if(mockResponces.isEmpty()){
                List<TargetPainterPictureInfo> list = loadInfo(context, gson);
                for(TargetPainterPictureInfo info : list){
                    mockResponces.put(info.getUid(), info.getInfo());

                }
            }
            if (!mockResponces.containsKey(uid))
                throw new IllegalStateException("Failed market to regonize with uid:  " + uid);
            return mockResponces.get(uid);
        }
    }


    private static List<TargetPainterPictureInfo> loadInfo(Context context, Gson gson){

        Device.throwIfOnMainThread();
        Timber.d("Panorama. REST : load panoramas");
        Resources res = context.getResources();
        Reader reader = null;
        try {
            reader = new InputStreamReader(res.openRawResource(R.raw.responces));
            Type listType = new TypeToken<ArrayList<TargetPainterPictureInfo>>() {}.getType();
            List<TargetPainterPictureInfo> panoramas = gson.fromJson(reader, listType);
            return panoramas;
        }
        finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }

    }


}
