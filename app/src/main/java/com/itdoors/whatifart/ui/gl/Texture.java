/*===============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States 
and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
===============================================================================*/

package com.itdoors.whatifart.ui.gl;

import android.graphics.Bitmap;

import com.itdoors.whatifart.ar.Sizes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// Support class for the Vuforia samples applications.
// Exposes functionality for loading a texture from the APK.
public final class Texture {

    private final int mWidth, mHeight;  //width and height in bitmap

    private final int mChannels;       // The number of channels.
    private final ByteBuffer mData;    // The pixel data.
    private final int[] mTextureID = new int[1];

    private final boolean mSuccess;

    private final Sizes size;

    public Texture(int w, int h, Sizes size, int channels, ByteBuffer data, boolean success) {

        this.mWidth = w;
        this.mHeight = h;
        this.mChannels = channels;
        this.mData = data;
        this.mSuccess = success;

        this.size = size;

    }

    public ByteBuffer getData() {
        return mData;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public int[] getTextureID() {
        return mTextureID;
    }

    public int getChannels() {
        return mChannels;
    }
    public boolean getSuccess(){
        return mSuccess;
    }

    public Sizes getSize() {
        return size;
    }

    public static Texture generateTexture(Bitmap bitmap, Sizes size){

        //TODO max possible resolution for big images. Connect to real size
        int w = bitmap.getWidth(), h = bitmap.getHeight();
        int rowSize = w * 4;

        ByteBuffer textureData = ByteBuffer.allocateDirect(w * h * 4).order(ByteOrder.nativeOrder());
        Texture texture = new Texture(w, h, size, 4, textureData, true);

        for(int row = h - 1 ; row >= 0; --row){

            int[] rowData = new int[w];
            bitmap.getPixels(rowData, 0 , w, 0, row, w, 1);

            byte[] dataBytes = new byte[w * 4];
            for (int pixel = 0; pixel < rowData.length; ++pixel)
            {
                int colour = rowData[pixel];
                dataBytes[pixel * 4]     = (byte) (colour >>> 16); // R
                dataBytes[pixel * 4 + 1] = (byte) (colour >>> 8);  // G
                dataBytes[pixel * 4 + 2] = (byte)  colour;         // B
                dataBytes[pixel * 4 + 3] = (byte) (colour >>> 24); // A
            }
            texture.mData.put(dataBytes, 0 , rowSize);
        }

        texture.mData.rewind();
        return texture;
    }

}
