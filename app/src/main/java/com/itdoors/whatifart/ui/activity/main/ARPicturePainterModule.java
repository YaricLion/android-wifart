package com.itdoors.whatifart.ui.activity.main;

import com.itdoors.whatifart.AppModule;
import com.itdoors.whatifart.ui.dialog.AboutProgramFragment;
import com.itdoors.whatifart.ui.dialog.AboutWIFFragment;
import com.itdoors.whatifart.ui.dialog.screenshot.ScreenShotDialogFragment;
import com.itdoors.whatifart.ui.view.VersionView;
import com.itdoors.whatifart.util.ViewSubscriptions;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module (
        injects = {
                MainARActivity.class,
                ScreenShotDialogFragment.class,
                AboutWIFFragment.class,
                VersionView.class,
                AboutProgramFragment.class
        },
        addsTo = AppModule.class,
        library = true
)
public final class ARPicturePainterModule {

    private final MainARActivity mainArActivity;

    public ARPicturePainterModule(MainARActivity mainArActivity) {
        this.mainArActivity = mainArActivity;
    }

    @Provides @Singleton
    MainARActivity provideActivity(){
        return mainArActivity;
    }

    @Provides @Singleton @Named("activity_onPauseSubscription") ViewSubscriptions provideActivityViewSubscriptions(){
        return new ViewSubscriptions(Schedulers.io(), AndroidSchedulers.mainThread());
    }

}
