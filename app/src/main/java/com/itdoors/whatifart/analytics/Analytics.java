package com.itdoors.whatifart.analytics;

import com.itdoors.whatifart.analytics.event.Event;

/**
 * Created by v014nd on 31.03.2016.
 */
public interface Analytics {
    void send(Event event);
}
