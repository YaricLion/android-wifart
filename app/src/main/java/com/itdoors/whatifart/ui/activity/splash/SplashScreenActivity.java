package com.itdoors.whatifart.ui.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.BaseActivity;
import com.itdoors.whatifart.ui.activity.main.MainARActivity;
import com.itdoors.whatifart.ui.activity.tutorial.TutorialActivity;
import com.itdoors.whatifart.util.Injector;
import com.itdoors.whatifart.util.prefs.BooleanPreference;

import javax.inject.Inject;
import javax.inject.Named;

public class SplashScreenActivity extends BaseActivity {

    @Inject @Named("isFirstLaunch") BooleanPreference isFistLaunchPrefs;

    private final int DELAY = 4000;

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.obtain(getApplication()).inject(this);
        setContentView(R.layout.activity_splash);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override protected void onResume() {
        super.onResume();
        mainHandler.postDelayed(() -> {

            if (isFirstLaunch()) {
                goToTutorial();
            } else {
                goToMain();
            }

            isFistLaunchPrefs.set(false);
            finish();

        }, DELAY);

    }

    @Override protected void onPause() {
        super.onPause();
        mainHandler.removeCallbacksAndMessages(null);
    }

    private boolean isFirstLaunch(){
       return !isFistLaunchPrefs.isSet() || isFistLaunchPrefs.get();
    }

    private void goToTutorial(){
        Intent intent = new Intent(this, TutorialActivity.class);
        intent.putExtra(Intents.Tutorial.FIRST_LAUNCH, true);
        startActivity(intent);
    }

    private void goToMain(){
        Intent intent = new Intent(this, MainARActivity.class);
        startActivity(intent);
    }

}
