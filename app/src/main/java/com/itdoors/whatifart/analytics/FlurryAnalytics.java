package com.itdoors.whatifart.analytics;

import com.flurry.android.FlurryAgent;
import com.google.common.base.Strings;
import com.itdoors.whatifart.analytics.event.Event;

import java.util.Map;

public class FlurryAnalytics implements Analytics{

    @Override public void send(Event event) {
        if(event != null && Strings.isNullOrEmpty(event.getName())) {
            Map<String, String> params = event.getParams();
            if(params != null && !params.isEmpty()) {
                FlurryAgent.logEvent(event.getName(), event.getParams());
            }else{
                FlurryAgent.logEvent(event.getName());
            }
        }
    }

}
