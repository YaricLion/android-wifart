package com.itdoors.whatifart.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.permission.AllowCameraEvent;
import com.itdoors.whatifart.event.permission.DenyCameraEvent;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;


public class AppNeedCameraDialogFragment extends DialogFragment {

    @Inject EventBus eventBus;

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Injector.obtain(getActivity().getApplicationContext()).inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.permission_tutorial_rationale_dialog_title)
                .content(R.string.permission_tutorial_rationale_dialog_msg)
                .positiveText(R.string.permission_tutorial_rationale_allow)
                .positiveColorRes(R.color.red_500)
                .negativeText(R.string.permission_tutorial_rationale_deny)
                .negativeColorRes(R.color.blue_500)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        View negativeAction = dialog.getActionButton(DialogAction.NEGATIVE);

        positiveAction.setOnClickListener(view -> {
            eventBus.post(new AllowCameraEvent());
            dialog.dismiss();
        });

        negativeAction.setOnClickListener(view -> {
            eventBus.post(new DenyCameraEvent());
            dialog.dismiss();
        });

        return dialog;

    }


}
