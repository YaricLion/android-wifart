package com.itdoors.whatifart.crashlytics;


import android.util.Log;

public final class CrashReportingTree extends TreeWrapper {

    @Override protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.ERROR || priority == Log.ASSERT) {
            FailReport.logError(t, tag, message);
        }
    }

    @Override public void loggingError(int priority, Throwable logging, Throwable original, String message, Object... args) {
        FailReport.logLoggingError(logging, original, message, args);
    }

}