package com.itdoors.whatifart.data.api;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.Gson;
import com.itdoors.whatifart.ar.ARConfig;
import com.itdoors.whatifart.ar.Sizes;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.data.model.PainterResponse;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.TextureUtils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.schedulers.Schedulers;

public class PainterImageServiceImpl implements PainterImageService {

    private final Picasso picasso;
    private final PainterService service;

    private static volatile PainterImageServiceImpl singleton = null;

    public static PainterImageServiceImpl getInstance(Context context, Gson gson){
        if (singleton == null) {
            synchronized (PainterImageServiceImpl.class) {
                if (singleton == null) {
                    singleton = new PainterImageServiceImpl(Picasso.with(context), new PainterMockService(context, gson));
                }
            }
        }
        return singleton;
    }

    public PainterImageServiceImpl(Picasso picasso, PainterService service) {
        this.picasso = picasso;
        this.service = service;
    }

    @Override  public Observable<PainterResponse> getPainterPicturesInfo(String uid) {
        return service.getPainterInfo(uid).flatMap(painterPicturesInfo -> {

                    Picture picture = painterPicturesInfo.getPicture();
                    Painter painter = painterPicturesInfo.getPainter();

                    Sizes realPictureSize = new Sizes(picture.getRealW(), picture.getRealH());

                    final Sizes pictureSize = TextureUtils.resizeIfNeed(realPictureSize);
                    final Sizes painterSize = new Sizes(ARConfig.painterLogoSize, ARConfig.painterLogoSize); //def painter image size

                    return Observable.zip(
                            loadBitmap(() -> loadPainterBitmap(picasso, painter.getImageLink(), painterSize) ),
                            loadBitmap(() -> loadPictureBitmap(picasso, picture.getImageLink(), pictureSize)),
                            (painterBitmap, pictureBitmap) -> new PainterResponse(painterPicturesInfo, painterBitmap, pictureBitmap));
                }
        );
    }

    private static Observable<Bitmap> loadBitmap(Callable<Bitmap> loadBitmapAction) {
        Observable<Bitmap> fileObservable = Observable.create(observer -> {
            try {
                if (!observer.isUnsubscribed()) {
                    Bitmap bitmap = loadBitmapAction.call();
                    observer.onNext(bitmap);
                    observer.onCompleted();
                }
            } catch (Exception e) {
                observer.onNext(null);
                observer.onCompleted();
                //observer.onError(e);
            }
        });
        return Observable.defer(() -> fileObservable).subscribeOn(Schedulers.io());
    }

    private static Bitmap loadPictureBitmap(Picasso picasso, String url, Sizes expected) throws IOException{
        Device.throwIfOnMainThread();
        return picasso.load(url)
               .memoryPolicy(MemoryPolicy.NO_CACHE)
               .resize((int) expected.getW(), (int) expected.getH())
               .get();
    }

    private static Bitmap loadPainterBitmap(Picasso picasso, String url, Sizes expected) throws IOException{
        Device.throwIfOnMainThread();
        return picasso.load(url)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .centerCrop()
                .resize((int) expected.getW(), (int) expected.getH())
                .get();
    }

}
