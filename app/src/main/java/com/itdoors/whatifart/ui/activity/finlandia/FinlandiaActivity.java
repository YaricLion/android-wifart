package com.itdoors.whatifart.ui.activity.finlandia;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.itdoors.whatifart.BuildConfig;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.api.PainterService;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.data.model.PainterPicturesInfo;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.event.ShowPainterImageEvent;
import com.itdoors.whatifart.event.ShowPictureImageEvent;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.YoutubeActivity;
import com.itdoors.whatifart.ui.activity.painter.PainterImageActivity;
import com.itdoors.whatifart.ui.activity.picture.PictureImageActivity;
import com.itdoors.whatifart.ui.view.custom.BetterViewAnimator;
import com.itdoors.whatifart.ui.view.custom.DynamicWidthImageView;
import com.itdoors.whatifart.util.AnimationHelper;
import com.itdoors.whatifart.util.CircleStrokeTransformation;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.Injector;
import com.itdoors.whatifart.util.ShareHelper;
import com.itdoors.whatifart.util.ViewSubscriptions;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class FinlandiaActivity extends YoutubeActivity implements YouTubePlayer.OnFullscreenListener{

    public static final String FINLANDIA = "finlandia";
    private static final String FINLANDIA_VIDEO_ID = "gv6yrr4SSg8";

    @Bind(R.id.picture_detail_animator)  BetterViewAnimator animatorView;
    @Bind(R.id.picture_detail_retry)     Button retryBtn;

    @Bind(R.id.picture_detail_painter_image) ImageView mPainterImageView;
    @Bind(R.id.picture_detail_painter_name)  TextView mPainterNameView;
    @Bind(R.id.picture_detail_year)          TextView yearView;

    @Bind(R.id.picture_detail_picture_description)   TextView pictureDescriptionView;
    @Bind(R.id.picture_detail_painter_description)   TextView painterDescriptionView;
    @Bind(R.id.picture_detail_price)                 TextView picturePriceView;
    @Bind(R.id.picture_detail_painter_site)          TextView painterSiteView;

    @Bind(R.id.picture_detail_painter_container) View painterHolderView;
    @Bind(R.id.picture_detail_painter_panel)     View painterHolderPanel;
    @Bind(R.id.picture_detail_title_description) TextView picturenameView;

    @Bind(R.id.action_picture_detail_share)              ImageView pictureShareView;
    @Bind(R.id.action_picture_detail_painter_facebook)   ImageView painterFacebookView;

    @Bind(R.id.action_picture_detail_back) View backBtn;

    @BindString(R.string.without_name) String withoutName;

    private CircleStrokeTransformation transformation;

    @Inject PainterService service;
    @Inject Device device;
    @Inject Picasso picasso;
    @Inject EventBus eventBus;
    @Inject UrlValidator urlValidator;

    private final ViewSubscriptions subscriptions = new ViewSubscriptions(Schedulers.io(), AndroidSchedulers.mainThread());

    private Picture picture;
    private Painter painter;
    private String uid;

    private boolean needToLoad = false;

    @Bind(R.id.view_finlandia_youtube)    YouTubePlayerView youTubeView;

    private YouTubePlayer player;
    private boolean fullscreen = false;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.obtain(getApplicationContext()).inject(this);
        setContentView(R.layout.activity_finlandia);
        ButterKnife.bind(this);
        transformation = new CircleStrokeTransformation(this, getResources().getColor(android.R.color.transparent), 0);
        youTubeView.initialize(BuildConfig.YOUTUBE_DEVELOPER_API_KEY, this);
        setup(getUid());
        fill();
    }

    private void fill(){
        if (!needToLoad){
            fill(this.picture, this.painter);
        }
        else{
            load();
        }
    }

    private String getUid(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String uid = bundle.getString(Intents.Picture.PICTURE_ID);
            if (!Strings.isNullOrEmpty(uid)) {
                return uid;
            }
        }
        return null;
    }

    @Override protected void onDestroy() {
        ButterKnife.unbind(this);
        subscriptions.unsubscribe();
        super.onDestroy();
    }

    @Override public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;
        player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        player.setOnFullscreenListener(this);
        if (!wasRestored) {
            player.cueVideo(FINLANDIA_VIDEO_ID);
        }
    }

    @Override protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Nullable   @OnClick(R.id.about_app_site_link) public void onSiteClicked(View v){
        String url = getResources().getString(R.string.app_site_link);
        Device.startBrowserLink(this, url);
    }

    @Override public void onBackPressed() {
        if(player != null && fullscreen)
            player.setFullscreen(false);
        else
            super.onBackPressed();
    }

    @Override public void onFullscreen(boolean isFullscreen) {
        fullscreen = isFullscreen;
    }

    public Action1<PainterPicturesInfo> loadFinishAction = response -> {

        Optional<PainterPicturesInfo> item = Optional.fromNullable(response);
        Optional<Picture> picture = Optional.fromNullable(item.isPresent() ? item.get().getPicture() : null);
        Optional<Painter> painter = Optional.fromNullable(item.isPresent()? item.get().getPainter(): null);

        if(picture.isPresent() && painter.isPresent()){
            setup(picture.get(), painter.get());
            fill(picture.get(), painter.get());
            animatorView.setDisplayedChildId(R.id.picture_detail_content);
        }
        else{
            animatorView.setDisplayedChildId(R.id.picture_detail_error);
        }
    };

    private Action1<Throwable> loadErrorAction = throwable -> {
        Timber.e(throwable, "FAIL TO SHOW PICTURE");
        animatorView.setDisplayedChildId(R.id.picture_detail_error);
    };

    private void load(){
        Timber.d("load()");
        subscriptions.add(getInfo(), loadFinishAction, loadErrorAction);
    }

    private void retry(){
        animatorView.setDisplayedChildId(R.id.picture_detail_progress);
        load();
    }

    private Observable<PainterPicturesInfo> getInfo(){
        return service.getPainterInfo(uid);
    }

    public void setup(Picture picture, Painter painter) {
        if(picture != null && painter != null) {
            needToLoad = false;
            this.uid = String.valueOf(picture.getId());
            this.picture = picture;
            this.painter = painter;
        }
    }

    public void setup(String pictureId) {
        if(pictureId != null) {
            needToLoad = true;
            this.uid = pictureId;
        }
    }

    private void fill(Picture picture, Painter painter){

        if(picture != null && painter != null) {

            String pictureLink = picture.getImageLink();

            Optional<String> pictureName = Optional.fromNullable(picture.getName());
            String pictureNameInfo = pictureName.or(withoutName);
            picturenameView.setText(pictureNameInfo);

            String priceInfo = null;
            showIfNotEmptyOrGone(picturePriceView, priceInfo);

            String painterLink = painter.getImageLink();
            picasso.load(painterLink)
                    .placeholder(R.drawable.contact_icon)
                    .error(R.drawable.contact_icon)
                    .fit()
                    .centerCrop()
                    .transform(transformation)
                    .into(mPainterImageView);

            Optional<String> painterName = Optional.fromNullable(painter.getName());
            Optional<String> painterSurname = Optional.fromNullable(painter.getSurname());

            StringBuilder nameBuilder = new StringBuilder();
            if(painterName.isPresent())
                nameBuilder.append(painterName.get());
            if(painterSurname.isPresent())
                nameBuilder.append(" ").append(painterSurname.get());

            String name = nameBuilder.toString();
            mPainterNameView.setText(name);

            Optional<String> painterDescription = Optional.fromNullable(painter.getDescription());
            Optional<String> pictureDescription = Optional.fromNullable(picture.getDescription());

            String painterDescriptionInfo = painterDescription.orNull();
            String pictureDescriptionInfo = pictureDescription.orNull();
            String yearInfo = picture.getYear() > 0 ? String.valueOf(picture.getYear()) : "";

            showHtmlIfNotEmptyOrGone(pictureDescriptionView, pictureDescriptionInfo);
            showHtmlIfNotEmptyOrGone(painterDescriptionView, painterDescriptionInfo);

            yearView.setText(yearInfo);

            site(painterSiteView, painter.getSite());
            facebook(painterFacebookView, painter.getFacebook());

        } else {
            Timber.e("Picture is null -" + (picture == null) + "\n" + "Painter is null - " + (painter == null));
        }
        animatorView.setDisplayedChildId(R.id.picture_detail_content);

    }

    private static void showHtmlIfNotEmptyOrGone(TextView view, String html) {
        if (!StringUtils.isEmpty(html)) {
            view.setVisibility(View.VISIBLE);
            view.setText(Html.fromHtml(html));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private static void showIfNotEmptyOrGone(TextView view, String info) {
        if(!StringUtils.isEmpty(info)) {
            view.setVisibility(View.VISIBLE);
            view.setText(info);
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private void site(View view, String site){
        if(!StringUtils.isEmpty(site) && urlValidator.isValid(site)){
            view.setVisibility(View.VISIBLE);
            view.setOnClickListener(v -> Device.startBrowserLink(this, site));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private void facebook(View view, String facebook){
        if(!StringUtils.isEmpty(facebook) && urlValidator.isValid(facebook)){
            view.setVisibility(View.VISIBLE);
            view.setOnClickListener(v ->
                    AnimationHelper.pulse(v, () ->
                                    Device.openFacebookPage(this, facebook)
                    ));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.action_picture_detail_share) void share(View v){
        AnimationHelper.pulse(v, () -> {
            ShareHelper.sharePicture(this, picture, urlValidator);
        });
    }

    @OnClick(R.id.picture_detail_painter_image) void showPainter(View v){
        eventBus.post(new ShowPainterImageEvent(painter));
    }

    @Override public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override public void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    public void onEventMainThread(ShowPainterImageEvent event){
        Intent intent = new Intent(this, PainterImageActivity.class);
        intent.putExtra(Intents.Painter.PAINTER, event.getPainter());
        startActivity(intent);
    }

    @Override protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
