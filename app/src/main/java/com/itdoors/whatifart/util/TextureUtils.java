package com.itdoors.whatifart.util;

import com.itdoors.whatifart.ar.ARConfig;
import com.itdoors.whatifart.ar.Sizes;

import timber.log.Timber;

public final class TextureUtils {

    private TextureUtils(){
        throw new AssertionError("No instances!");
    }

    public static Sizes resizeIfNeed(Sizes size){

        Timber.d("ScaleTexture. START! ");

        float df = (float) ARConfig.frameSize;

        float maxPicW = (float) ARConfig.maxPicW - 2 * df, maxPicH = (float) ARConfig.maxPicH - 2 * df;
        float minPicW = (float) ARConfig.minPicW - 2 * df, minPicH = (float) ARConfig.minPicH - 2 * df;

        Timber.d("ScaleTexture. maxPicW = " + maxPicW + "; maxPicH = " + maxPicH);
        Timber.d("ScaleTexture. minPicW = " + minPicW + "; minPicH = " + minPicH);

        float picW = size.getW(), picH = size.getH();

        Timber.d("ScaleTexture. picW = " + picW + "; picH = " + picH);

        boolean outsideMaxW = ( picW > maxPicW ), outsideMaxH = ( picH > maxPicH );
        boolean insideMinW  = ( picW < minPicW ), insideMinH  = ( picH < minPicH );

        float newPicW = picW, newPicH = picH;

        if( outsideMaxW || outsideMaxH){ //outside "max-rectangle"

            Timber.d("ScaleTexture. outsideMaxW || outsideMaxH = true");

            if(outsideMaxW && outsideMaxH) {

                Timber.d("ScaleTexture. outsideMaxW && outsideMaxH = true");

                float a = maxPicH, b = maxPicW;
                float c = picH,    d = picW;

                if( d * a / c < b ){

                    Timber.d("1 . ScaleTexture. d * a / c < b= true");
                    newPicW = d * a / c;
                    newPicH = a;

                } else {

                    Timber.d("2. ScaleTexture. d * a / c < b = false");

                    newPicW = b;
                    newPicH = b * c / d;
                }

            } else if(!outsideMaxW){

                Timber.d("3. ScaleTexture. !outsideMaxW = true");
                newPicW = picW  / picH * maxPicH ;
                newPicH = maxPicH ;

            } else {

                Timber.d("4. ScaleTexture. !outsideMaxW = false");
                newPicH = picH / picW * maxPicW;
                newPicW = maxPicW;

            }

        } else if( insideMinW && insideMinH ){ //inside "min-rectangle"

            Timber.d("ScaleTexture. insideMinW && insideMinH = true");
            float tangAlpha = minPicW / minPicH, tangBetta = picW / picH;

            if( tangAlpha >= tangBetta ) {

                Timber.d("5. ScaleTexture. tangAlpha >= tangBetta  = true");
                newPicW = picW / picH * minPicH;
                newPicH = minPicH;

            }
            else{

                Timber.d("6. ScaleTexture. tangAlpha >= tangBetta  = false");
                newPicH = picH / picW * minPicW ;
                newPicW = minPicW;

            }
        }

        Timber.d("ScaleTexture. newPicW = " + newPicW + "; newPicH = " + newPicH);
        Timber.d("ScaleTexture. END! ");

        return new Sizes(newPicW, newPicH);
    }
}
