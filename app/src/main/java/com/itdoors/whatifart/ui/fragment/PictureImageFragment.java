package com.itdoors.whatifart.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.ui.activity.FragmentContainer;
import com.itdoors.whatifart.ui.view.picture.PictureImageView;
import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import timber.log.Timber;

public class PictureImageFragment  extends ContainerFragment<PictureImageView>  {

    private static final String PICTURE_TAG = "PictureImageFragment.PICTURE_TAG";
    private static final String PICTURE_URL_TAG = "PictureImageFragment.PICTURE_URL_TAG";

    public static PictureImageFragment newInstance(Picture picture){
        PictureImageFragment fragment = new PictureImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(PICTURE_TAG, picture);
        fragment.setArguments(args);
        return fragment;
    }

    public static PictureImageFragment newInstance(String url){

        PictureImageFragment fragment = new PictureImageFragment();
        Bundle args = new Bundle();
        args.putString(PICTURE_URL_TAG, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override protected int getViewId() {
        return R.layout.view_picture_image;
    }

    @Override protected void setupView(PictureImageView view){
        Bundle args = getArguments();
        if(args != null) {
            String url = args.getString(PICTURE_URL_TAG);
            if(url != null){
                view.setup(url);
                return;
            }
            Picture picture = args.getParcelable(PICTURE_TAG);
            if(picture != null ) {
                view.setup(picture);
                return;
            }
        }
        throw new IllegalStateException("Failed PictureImageFragment setup!");
    }

}
