package com.itdoors.whatifart.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.util.PictureShareUtils;

import java.io.File;

import rx.Observable;

/**
 * Created by v014nd on 26.03.2016.
 */
public class ScreenShotServiceImpl implements ScreenShotService {

    private final static String PICTURES_PATH_NAME = "WhatIfARt";
    private final Context context;

    private static volatile com.itdoors.whatifart.ui.ScreenShotServiceImpl singleton = null;

    public ScreenShotServiceImpl(Context context) {
        this.context = context;
    }

    public static com.itdoors.whatifart.ui.ScreenShotServiceImpl getInstance(Context context) {
        if (singleton == null) {
            synchronized (com.itdoors.whatifart.ui.ScreenShotServiceImpl.class) {
                if (singleton == null) {
                    singleton = new com.itdoors.whatifart.ui.ScreenShotServiceImpl(context);
                }
            }
        }
        return singleton;
    }

    @Override public Observable<Intent> share(Bitmap bm, Picture pic) {
        return save(bm, pic).flatMap(file -> Observable.just(PictureShareUtils.getSharePictureIntent(file)));
    }

    @Override public Observable<File> save(Bitmap bm, Picture pic) {

        Observable<File> fileObservable = Observable.create(observer -> {
            try {
                if (!observer.isUnsubscribed()) {
                    File file = PictureShareUtils.saveImageToPictures(context, PICTURES_PATH_NAME, pic, bm);
                    PictureShareUtils.addToContentResolver(context, file, file.getName());
                    observer.onNext(file);
                    observer.onCompleted();
                }
            } catch (Exception e) {
                observer.onError(e);
            }
        });
        return Observable.defer(() -> fileObservable);
    }

}
