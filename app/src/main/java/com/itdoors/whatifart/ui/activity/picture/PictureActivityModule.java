package com.itdoors.whatifart.ui.activity.picture;

import com.itdoors.whatifart.AppModule;
import com.itdoors.whatifart.ui.dialog.VersionFragment;
import com.itdoors.whatifart.ui.dialog.screenshot.ScreenShotDialogFragment;
import com.itdoors.whatifart.ui.fragment.PictureFragment;
import com.itdoors.whatifart.ui.view.VersionView;
import com.itdoors.whatifart.ui.view.picture.PictureView;
import com.itdoors.whatifart.util.ViewSubscriptions;
import com.qualcomm.vuforia.samples.Books.app.Books.Shaders;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module(
        injects = {
                PictureActivity.class,
                PictureFragment.class,
                PictureView.class
        },
        addsTo = AppModule.class,
        library = true
)

public class PictureActivityModule {

    private final PictureActivity pictureActivity;

    public PictureActivityModule(PictureActivity pictureActivity) {
        this.pictureActivity = pictureActivity;
    }

    @Provides @Singleton PictureActivity provideActivity(){
        return pictureActivity;
    }

    @Provides @Singleton @Named("PictureViewSubscription") ViewSubscriptions providePictureViewSubscriptions () {
        return new ViewSubscriptions(Schedulers.io(), AndroidSchedulers.mainThread());
    }

}
