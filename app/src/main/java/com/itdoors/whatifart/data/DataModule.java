package com.itdoors.whatifart.data;


import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itdoors.whatifart.data.api.ApiModule;
import com.itdoors.whatifart.util.prefs.BooleanPreference;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import org.apache.commons.validator.routines.UrlValidator;
import org.joda.time.DateTime;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;

@Module(
        includes = ApiModule.class,
        complete = false,
        library = true
)
public final class DataModule {

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    @Provides @Singleton SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("wif", MODE_PRIVATE);
    }

    @Provides @Singleton Gson provideGson() {
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        return gson.create();
    }

    @Provides @Singleton OkHttpClient provideOkHttpClient(Application app) {
        return createOkHttpClient(app);
    }

    @Provides @Singleton LruCache provideMemoryLruCache(Application app){
        return new LruCache(app);
    }

    @Provides @Singleton Picasso providePicasso(Application app, OkHttpClient client, LruCache lruMemoryCache) {
        return new Picasso.Builder(app)
                .downloader(new OkHttpDownloader(client))
                .memoryCache(lruMemoryCache)
                .listener((picasso, uri, e) -> {
                    Timber.e(e, "Failed to load image: %s", uri);
                })
                .build();
    }

    @Provides @Singleton UrlValidator provideUrlValidator(){
        return UrlValidator.getInstance();
    }

    static OkHttpClient createOkHttpClient(Application app) {

        OkHttpClient client = new OkHttpClient();
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        client.setCache(cache);

        return client;
    }

}
