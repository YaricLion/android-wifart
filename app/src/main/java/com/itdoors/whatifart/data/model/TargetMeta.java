package com.itdoors.whatifart.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public final class TargetMeta implements Parcelable {

    @SerializedName("uid")
    private final String uid;

    public TargetMeta(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TargetMeta that = (TargetMeta) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(uid, that.uid)
                .isEquals();
    }

    @Override public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(uid)
                .toHashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
    }

    protected TargetMeta(Parcel in) {
        this.uid = in.readString();
    }

    public static final Parcelable.Creator<TargetMeta> CREATOR = new Parcelable.Creator<TargetMeta>() {
        public TargetMeta createFromParcel(Parcel source) {
            return new TargetMeta(source);
        }

        public TargetMeta[] newArray(int size) {
            return new TargetMeta[size];
        }
    };
}
