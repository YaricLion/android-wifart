package com.itdoors.whatifart.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.itdoors.whatifart.util.Injector;

import javax.inject.Inject;

import dagger.ObjectGraph;

public abstract class ContainerActivity<FRAGMENT extends Fragment> extends BaseActivity{

    @Inject ActivityContainer appContainer;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getObjectGraph().inject(this);
        appContainer.setContainer(this);
        if (savedInstanceState == null) {
            Fragment fragment = setupFragment();
            if(fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .add(appContainer.container(), fragment)
                        .commit();
            }
            else{
                finish();
            }
        }
    }

    protected ObjectGraph getObjectGraph(){
        return Injector.obtain(getApplication());
    }

    protected abstract FRAGMENT setupFragment();

}
