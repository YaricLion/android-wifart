package com.itdoors.whatifart.ar;


import timber.log.Timber;

public final class ARConfig {

    public static final int glPlanePow2Size;
    public static final int frameSize;
    public static final int painterLogoSize;

    public static final int textureW;
    public static final int textureH;
    public static final int minPicW;
    public static final int minPicH;
    public static final int maxPicW;
    public static final int maxPicH;

    public static final int painterPictureNameTextSize;
    public static final int pictureNameTextSize;
    public static final int painterAdressTextSize;
    public static final int painterDesrcTextSize;
    public static final int painterPictureInfoHolderMargin;

    private static final float minWRatio = 3f/8;
    private static final float minHRatio = 2f/3;
    private static final float maxWRatio = 1f/2;
    private static final float maxHRatio = 8f/9;
    private static final float frameWRatio = 1f/40;
    private static final float painterWRatio = 3f/16;
    private static final float picture_name_text_size_w_ratio             = 45f / 1600;
    private static final float painter_picture_name_text_size_w_ratio     = 45f / 1600;
    private static final float painter_adress_text_size_w_ratio           = 40f / 1600;
    private static final float painter_desrc_text_size_w_ratio            = 42f / 1600;
    private static final float painter_picture_info_holder_margin_w_ratio = 40f / 1600;

    private ARConfig(){
        throw new AssertionError("No instances!");
    }

    static {

       int heapMb = (int)( Runtime.getRuntime().maxMemory() / (1024 * 1024) );

       HeapType heapType = HeapType.getType(heapMb);

       glPlanePow2Size = getEmptyBufferSize(heapType);

       Sizes sizes = getTextureSize(heapType);

       textureW = (int) sizes.getW();
       textureH = (int) sizes.getH();

       minPicW =  (int)( minWRatio * textureW );
       minPicH =  (int)( minHRatio * textureH );

       maxPicW =  (int)( maxWRatio * textureW );
       maxPicH =  (int)( maxHRatio * textureH );

       frameSize       = (int) (frameWRatio   * textureW );
       painterLogoSize = (int) (painterWRatio * textureW);

       painterPictureNameTextSize     = (int) (painter_picture_name_text_size_w_ratio     * textureW);
       pictureNameTextSize            = (int) (picture_name_text_size_w_ratio             * textureW);
       painterAdressTextSize          = (int) (painter_adress_text_size_w_ratio           * textureW);
       painterDesrcTextSize           = (int) (painter_desrc_text_size_w_ratio            * textureW);
       painterPictureInfoHolderMargin = (int) (painter_picture_info_holder_margin_w_ratio * textureW);

       Timber.d("pow2: " + glPlanePow2Size );
       Timber.d("heap: " + heapMb +  ", " + heapType);

    }

    public enum HeapType {

        SMALL, NORMAL, LARGE, XLARGE;

        public static HeapType getType(int heap){
            if(heap < 48)
                return SMALL;
            else if(heap >= 48 && heap <=64 )
                return NORMAL;
            else if(heap >64 && heap <= 128)
                return LARGE;
            else return XLARGE;
        }

        @Override public String toString() {
            switch (this) {
                case SMALL:  return "SMALL";
                case NORMAL: return "NORAML";
                case LARGE:  return "LARGE";
                case XLARGE: return "XLARGE";
            }
            throw new AssertionError("Never happened");
        }
    }


    private static int getEmptyBufferSize(HeapType type){

        switch (type) {
            case SMALL:
            case NORMAL:
                return 1024;
            default:
                return 2048;
        }

    }

    private static Sizes getTextureSize(HeapType type){

        int w,h;

        switch (type) {
            case SMALL:
            case NORMAL:
                w = 1024;
                h = 576;
                break;
            default:
                w = 1600;
                h = 900;
                break;
        }

        return new Sizes(w,h);

    }

}
