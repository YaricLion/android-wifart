package com.itdoors.whatifart.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class Picture implements Parcelable {

    @SerializedName("id")
    private final String id;
    @SerializedName("name")
    private final String name;
    @SerializedName("imageLink")
    private final String imageLink;
    @SerializedName("year")
    private final int year;
    @SerializedName("medium")
    private final String medium;
    @SerializedName("wight")
    private final int realW;
    @SerializedName("height")
    private final int realH;
    @SerializedName("shareLink")
    private final String shareLink;
    @SerializedName("price")
    private final int price;
    @SerializedName("description")
    private final String description;


    public Picture(String id, String name, String imageLink, int year, String medium, int realW, int realH, String shareLink, int price, String description) {
        this.id = id;
        this.name = name;
        this.imageLink = imageLink;
        this.year = year;
        this.medium = medium;
        this.realW = realW;
        this.realH = realH;
        this.shareLink = shareLink;
        this.price = price;
        this.description = description;
    }

    public int getRealH() {
        return realH;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getRealW() {
        return realW;
    }

    public int getYear() {
        return year;
    }

    public String getId() {
        return id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getMedium() {
        return medium;
    }

    public String getName() {
        return name;
    }

    public String getShareLink() {
        return shareLink;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Picture picture = (Picture) o;

        return new EqualsBuilder()
                .append(year, picture.year)
                .append(realW, picture.realW)
                .append(realH, picture.realH)
                .append(price, picture.price)
                .append(id, picture.id)
                .append(name, picture.name)
                .append(imageLink, picture.imageLink)
                .append(medium, picture.medium)
                .append(shareLink, picture.shareLink)
                .append(description, picture.description)
                .isEquals();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(imageLink)
                .append(year)
                .append(medium)
                .append(realW)
                .append(realH)
                .append(shareLink)
                .append(price)
                .append(description)
                .toHashCode();
    }

    @Override public String toString() {
        return "Picture{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", year=" + year +
                ", medium='" + medium + '\'' +
                ", realW=" + realW +
                ", realH=" + realH +
                ", shareLink='" + shareLink + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }


    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.imageLink);
        dest.writeInt(this.year);
        dest.writeString(this.medium);
        dest.writeInt(this.realW);
        dest.writeInt(this.realH);
        dest.writeString(this.shareLink);
        dest.writeInt(this.price);
        dest.writeString(this.description);
    }

    protected Picture(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.imageLink = in.readString();
        this.year = in.readInt();
        this.medium = in.readString();
        this.realW = in.readInt();
        this.realH = in.readInt();
        this.shareLink = in.readString();
        this.price = in.readInt();
        this.description = in.readString();
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>() {
        public Picture createFromParcel(Parcel source) {
            return new Picture(source);
        }

        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };
}
