/*===============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States 
and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
===============================================================================*/

package com.itdoors.whatifart.ui.gl;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Looper;

import com.itdoors.whatifart.ar.ARConfig;
import com.itdoors.whatifart.ar.Sizes;
import com.itdoors.whatifart.event.ScreenShotReadyEvent;
import com.itdoors.whatifart.ui.activity.main.MainARActivity;
import com.qualcomm.vuforia.ImageTarget;
import com.qualcomm.vuforia.Matrix34F;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Vec3F;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.samples.Books.app.Books.Plane;
import com.qualcomm.vuforia.samples.Books.app.Books.Shaders;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationSession;
import com.qualcomm.vuforia.samples.SampleApplication.utils.SampleUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicInteger;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import de.greenrobot.event.EventBus;
import timber.log.Timber;


// The renderer class for the Books sample.
public class PainterInfoRenderer implements GLSurfaceView.Renderer {

    private final SampleApplicationSession vuforiaAppSession;
    private final MainARActivity activity;

    //Renderer states
    public static final int RS_NORMAL = 0;
    public static final int RS_TEXTURE_GENERATED = 4;
    public static final int RS_SCANNING = 5;

    // Initialize RenderState
    private volatile int renderState = RS_SCANNING;
    private volatile boolean mIsActive = false;
    private volatile boolean deleteCurrentProductTexture = false;

    //Lock for texture
    private final Object textureLock = new Object();
    private Texture mProductTexture;

    public  final static int MAX_BEFORE_RENDERING_TRANSITION = 24;
    private final AtomicInteger framesToSkipBeforeRenderingTransition = new AtomicInteger(MAX_BEFORE_RENDERING_TRANSITION);

    private final static int MAX_BEFORE_SCAN = 24;
    private final AtomicInteger framesToSkipBeforeStartScanning = new AtomicInteger(MAX_BEFORE_SCAN);

    //Confined to GL thread
    //////////////////////////////////////////////////////
    private int shaderProgramID;
    private int vertexHandle;
    private int normalHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;

    private float[] modelViewMatrix;

    private Matrix34F pose;
    private Plane mPlane;

    private int mScreenHeight;
    private int mScreenWidth;

    //////////////////////////////////////////////////////

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    /*
           Params to render texture
           Do not change the Plane params object. It depends on it.

           planeVertices[] =
           {
               -1f, -1f, 0f,
                1f, -1f, 0f,
                1f,  1f, 0f,
               -1f,  1f, 0f
           };

           (+1,-1)   (+1, +1)
            _______
           |       |
           |_______|

           (-1,-1)   (+1,-1)
   */

    private final int pow2       = ARConfig.glPlanePow2Size;
    private final int d          = ARConfig.frameSize;
    private final int textureW   = ARConfig.textureW;
    private final int textureH   = ARConfig.textureH;

    //EmptyBuffer for pow2 GL Image Texture
    private final ByteBuffer emptyBuff = ByteBuffer.allocateDirect(pow2 * pow2 * 4).order(ByteOrder.nativeOrder());

    private volatile boolean takeScreenShot = false;

    public PainterInfoRenderer(SampleApplicationSession appSession, MainARActivity context) {
        this.vuforiaAppSession = appSession;
        this.activity = context;
    }



    // Function for initializing the renderer.
    public void initRendering() {

        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f : 1.0f);

        // OpenGL setup for 3D model
        shaderProgramID = SampleUtils.createProgramFromShaderSrc(Shaders.cubeMeshVertexShader, Shaders.cubeFragmentShader);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexPosition");
        normalHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexNormal");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID, "modelViewProjectionMatrix");

        mPlane = new Plane();

    }

    // Called when the surface is created or recreated.
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        initRendering();

        Vuforia.onSurfaceCreated();
    }


    // Called when the surface changed size.
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        mScreenHeight = height;
        mScreenWidth = width;

        Vuforia.onSurfaceChanged(width, height);
    }


    // The render function.
    public void renderFrame() {

        // Clear color and depth buffer
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        // Get the state from Vuforia and mark the beginning of a rendering section
        State state = Renderer.getInstance().begin();

        // Explicitly render the Video Background
        Renderer.getInstance().drawVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);

        synchronized (textureLock) {

            if (deleteCurrentProductTexture) {
                if (mProductTexture != null) {
                    GLES20.glDeleteTextures(1, mProductTexture.getTextureID(), 0);
                    mProductTexture = null;
                }
                deleteCurrentProductTexture = false;
            }

        }

        // If the render state indicates that the texture is generated it generates
        // the OpenGL texture for start drawing the plane with the book data
        synchronized (textureLock) {
            if (renderState == RS_TEXTURE_GENERATED) {
                generateProductTextureInOpenGL();
            }
        }

        // Did we find any trackables this frame?
        if (state.getNumTrackableResults() > 0)
        {
            // If we are already tracking something we don't need
            // to wait any frame before starting the 2D transition
            // when the target gets lost
            framesToSkipBeforeRenderingTransition.set(0);

            // Gets current trackable result
            TrackableResult trackableResult = state.getTrackableResult(0);

            if (trackableResult == null)  return;

            modelViewMatrix = Tool.convertPose2GLMatrix(trackableResult.getPose()).getData();

            synchronized (trackableResult) {
                synchronized (textureLock) {
                    if (renderState == RS_NORMAL) {
                        if (mProductTexture != null) {
                            if(activity.isCurrentTarget(trackableResult)) {
                                renderAugmentation(trackableResult);
                            }
                        }
                    }
                }
                ////////////////////////////////////////////////////////////////////////////////

                if (renderState == RS_SCANNING) {

                    if (framesToSkipBeforeStartScanning.get() == 0) {

                        //Start loading texture
                        framesToSkipBeforeStartScanning.set(MAX_BEFORE_SCAN);
                        mainHandler.post(() -> {
                            activity.updateRendering(trackableResult);
                        });

                    } else {
                        framesToSkipBeforeStartScanning.decrementAndGet();
                    }
                }

            }
            ///////////////////////////////////////////////////////////////////////////////*/

        }
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        Renderer.getInstance().end();
    }

    //synchronized by textureLock when called
    private void renderAugmentation(TrackableResult trackableResult) {
        float[] modelViewProjection = new float[16];

        Trackable mTrackable = trackableResult.getTrackable();
        ImageTarget imagetarget = ((ImageTarget) mTrackable);
        Vec3F sizeVec = imagetarget.getSize();

        float targetW = sizeVec.getData()[0];
        float targetH = sizeVec.getData()[1];

        float scaleCoefficient = (targetW >= targetH) ? targetW : targetH;

        float scaleM11 = (scaleCoefficient / 2f );
        float scale = pow2 / scaleCoefficient,
              scaleM = scaleM11 *  scale;

        Sizes textureSizeInfo = mProductTexture.getSize();
        Sizes targetSizeInfo = new Sizes(targetW, targetH);

        float resizeScale = 1;
        if(!textureSizeInfo.equals(targetSizeInfo)){
            resizeScale = targetSizeInfo.getH() / textureSizeInfo.getH() ;
        }

        scaleM = scaleM * resizeScale;

        float translateX = resizeScale * ((float)textureW / 2f - d - textureSizeInfo.getW() / 2f);
        float translateY = resizeScale * (-1)* ((float)textureH / 2f - d - textureSizeInfo.getH() / 2f);

        Matrix.translateM(modelViewMatrix, 0, translateX, translateY, 0f);
        Matrix.scaleM(modelViewMatrix, 0, scaleM,  scaleM , 1.0f);

        // Applies 3d Transformations to the plane
        Matrix.multiplyMM(modelViewProjection, 0, vuforiaAppSession.getProjectionMatrix().getData(), 0, modelViewMatrix, 0);

        // Moves the trackable current position to a global variable used for
        // the 3d to 2D animation
        pose = trackableResult.getPose();

        Trackable trackable = trackableResult.getTrackable();
        Timber.e("Trackable", "Scale : " + scaleM +  " Trackable : [" + "name : " + trackable.getName() + ";" + "id: " + trackable.getId() + "]");

        // Shader Program for drawing
        GLES20.glUseProgram(shaderProgramID);

        // The 3D Plane is only drawn when the texture is loaded and generated
        if (renderState == RS_NORMAL && mProductTexture != null) {

            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false, 0, mPlane.getVertices());
            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false, 0, mPlane.getNormals());
            GLES20.glVertexAttribPointer(textureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, mPlane.getTexCoords());

            GLES20.glEnableVertexAttribArray(vertexHandle);
            GLES20.glEnableVertexAttribArray(normalHandle);
            GLES20.glEnableVertexAttribArray(textureCoordHandle);

            // Enables Blending State
            GLES20.glEnable(GLES20.GL_BLEND);

            ////////////////////////////////////////////////////////////////////////////////////////
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            ////////////////////////////////////////////////////////////////////////////////////////


            // Drawing Textured Plane
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mProductTexture.getTextureID()[0]);
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, modelViewProjection, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, mPlane.getIndices());

            GLES20.glDisableVertexAttribArray(vertexHandle);
            GLES20.glDisableVertexAttribArray(normalHandle);
            GLES20.glDisableVertexAttribArray(textureCoordHandle);

            // Disables Blending State - Its important to disable the blending
            // state after using it for preventing bugs with the Camera Video
            // Background
            GLES20.glDisable(GLES20.GL_BLEND);

            // Handles target re-acquisition - Checks if the overlay2D is shown
        }

        SampleUtils.checkGLError("Books renderFrame");

    }

    //synchronized by textureLock when called
    private void generateProductTextureInOpenGL() {

            // Generates the Texture in OpenGL
            GLES20.glGenTextures(1, mProductTexture.getTextureID(), 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mProductTexture.getTextureID()[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            ////////////////////////////////////////////////////////////////////////////////////////////
            // We create an empty power of two texture and upload a sub image.
            int width = mProductTexture.getWidth(), height = mProductTexture.getHeight();

            int xOffset = (pow2 - width) / 2;
            int yOffset = (pow2 - height) / 2;

            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, pow2, pow2, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, emptyBuff);
            GLES20.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, xOffset, yOffset, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mProductTexture.getData());
            // Updates the current Render State
            renderState = RS_NORMAL;
            SampleUtils.checkGLError("generateProductTextureInOpenGL");
    }


    // Called to draw the current frame.
    public void onDrawFrame(GL10 gl) {
        if (!mIsActive)
            return;

        renderFrame();

        if ( takeScreenShot ) {
            takeScreenShot = false;
            takeScreenShot(0,0, mScreenWidth, mScreenHeight);
        }

    }

    public void setRenderState(int state) {
        renderState = state;
    }

    public void deleteCurrentProductTexture() {
        deleteCurrentProductTexture = true;
    }

    public void setProductTexture(Texture texture) {
        synchronized (textureLock) {
            mProductTexture = texture;
        }
    }

    public void setActive(boolean active){
        this.mIsActive = active;
    }

    public void setFramesToSkipBeforeRenderingTransition(int framesToSkip) {
        framesToSkipBeforeRenderingTransition.set(framesToSkip);
    }

    public void takeScreenShot(){
        takeScreenShot = true;
    }

    private static Bitmap grabPixels(int x, int y, int w, int h) {

        int b[] = new int[w * (y + h)];
        int bt[] = new int[w * h];
        IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);

        GLES20.glReadPixels(x, 0, w, y + h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);

        for (int i = 0, k = 0; i < h; i++, k++) {
            for (int j = 0; j < w; j++) {
                int pix = b[i * w + j];
                int pb = (pix >> 16) & 0xff;
                int pr = (pix << 16) & 0x00ff0000;
                int pix1 = (pix & 0xff00ff00) | pr | pb;
                bt[(h - k - 1) * w + j] = pix1;
            }
        }

        Bitmap sb = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
        return sb;
    }

    private static void takeScreenShot(int x, int y, int w, int h){

        Bitmap bm = grabPixels(x,y,w,h);
        EventBus.getDefault().post( new ScreenShotReadyEvent(bm));

    }

}