package com.itdoors.whatifart.util.prefs;

import rx.Observable;

public interface RxPreference<T> {

    Observable<Void> set(T value);
    Observable<T> get();
    Observable<Boolean> isSet();
    Observable<Void> delete();

    class RxPreferenceImpl<T> implements RxPreference<T> {

        private final Preference<T> preference;

        public RxPreferenceImpl(Preference<T> preference) {
            this.preference = preference;
        }

        @Override public Observable<Void> set(T value) {
            return Observable.defer(() -> {
                preference.set(value);
                return Observable.just(null);
            });
        }

        @Override public Observable<T> get(){
            return Observable.defer(() -> Observable.just(preference.isSet() ? preference.get() : null));
        }

        @Override public Observable<Boolean> isSet() {
            return Observable.defer(() -> Observable.just(preference.isSet()));
        }

        @Override public Observable<Void> delete() {
            return Observable.defer(() -> {
                preference.delete();
                return Observable.just(null);
            });
        }
    }


}
