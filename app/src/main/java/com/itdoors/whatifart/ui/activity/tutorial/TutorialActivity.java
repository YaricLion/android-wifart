package com.itdoors.whatifart.ui.activity.tutorial;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.permission.AllowCameraEvent;
import com.itdoors.whatifart.event.permission.DenyCameraEvent;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.BaseActivity;
import com.itdoors.whatifart.ui.activity.main.MainARActivity;
import com.itdoors.whatifart.ui.dialog.AppNeedCameraDialogFragment;
import com.itdoors.whatifart.util.Injector;
import com.itdoors.whatifart.util.ToastUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.ObjectGraph;
import de.greenrobot.event.EventBus;
import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TutorialActivity extends BaseActivity {

    private static final int PERMISSIONS_REQUEST_CAMERA = 0;

    private static final int NUM_PAGES = 4;

    @Bind(R.id.tutorial_pager)    ViewPager pager;
    @Bind(R.id.tutorial_skip_btn) Button skip;
    @Bind(R.id.tutorial_done_btn) Button done;
    @Bind(R.id.tutorial_next_btn) ImageButton next;
    @Bind(R.id.circle_indicator)  CircleIndicator indicator;

    private boolean shouldShowCameraPermissionRationale = false;

    private PagerAdapter pagerAdapter;

    @Inject EventBus eventBus;

    @Override protected void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override protected void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        ObjectGraph appGraph = Injector.obtain(getApplication());
        appGraph.inject(this);

        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        if(getSupportActionBar() != null)
            getSupportActionBar().hide();

        skip.setOnClickListener(v -> endTutorial());
        next.setOnClickListener(v -> pager.setCurrentItem(pager.getCurrentItem() + 1, true));
        done.setOnClickListener(v -> endTutorial());

        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new CrossfadePageTransformer());
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override public void onPageSelected(int position) {
                if (position == NUM_PAGES - 2) {
                    skip.setVisibility(View.GONE);
                    next.setVisibility(View.GONE);
                    done.setVisibility(View.VISIBLE);
                } else if (position < NUM_PAGES - 2) {
                    skip.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                } else if (position == NUM_PAGES - 1) {
                    endTutorial();
                }
            }

            @Override public void onPageScrollStateChanged(int state) {}

        });

        indicator.setViewPager(pager);

    }


    private void endTutorial(){

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                end();
            }
            else {
                requestCamera();
            }
    }

    private void requestCamera(){
        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
    }

    private void showPermissionRationaleDialog(){
        AppNeedCameraDialogFragment fragment = new AppNeedCameraDialogFragment();
        fragment.show(getSupportFragmentManager(), "rationale");
    }

    public void onEventMainThread(AllowCameraEvent event){
        requestCamera();
    }

    public void onEventMainThread(DenyCameraEvent event){
        failEnd();
    }

    private void failEnd(){
        ToastUtils.ToastLong(getApplicationContext(), getString(R.string.permission_tutorial_rationale_dialog_denied_msg));
        finish();
    }

    private void end(){

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        boolean isFirstLaunch = getIntent().getBooleanExtra(Intents.Tutorial.FIRST_LAUNCH, false);
        if(isFirstLaunch) {
            Intent intent = new Intent(this, MainARActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            finish();
        }

    }

    @Override public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override public Fragment getItem(int position) {
            TutorialPane tp = null;
            switch(position){
                case 0: tp = TutorialPane.newInstance(R.layout.fragment_tutorial_one);
                    break;
                case 1: tp = TutorialPane.newInstance(R.layout.fragment_tutorial_two);
                    break;
                case 2: tp = TutorialPane.newInstance(R.layout.fragment_tutorial_three);
                    break;
                case 3: tp = TutorialPane.newInstance(R.layout.fragment_tutorial_transparent);
                    break;
            }
            return tp;
        }

        @Override public int getCount() {
            return NUM_PAGES;
        }
    }

    public class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @Override public void transformPage(View page, float position) {

            int pageWidth = page.getWidth();

            View text = page.findViewById(R.id.tutorial_slide_msg);
            View backgroundView = page.findViewById(R.id.tutorial_slide_bg_img);

            if (position <= 1) {
                page.setTranslationX(pageWidth * -position);
            }

            if(backgroundView != null) {
                //backgroundView.setTranslationX(0.75f * pageWidth * position);
                backgroundView.setAlpha(1.0f - Math.abs(position));
            }
            //Text both translates in/out and fades in/out
            if (text != null) {
                text.setTranslationX(pageWidth * position);
            }

        }

    }

    public static class TutorialPane extends Fragment {

        private final static String LAYOUT_ID = "layoutid";

        public static TutorialPane newInstance(int layoutId) {
            TutorialPane pane = new TutorialPane();
            Bundle args = new Bundle();
            args.putInt(LAYOUT_ID, layoutId);
            pane.setArguments(args);
            return pane;
        }

        @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(getArguments().getInt(LAYOUT_ID, -1), container, false);
        }

    }

    @Override protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                end();
            } else {
                // if permission was denied check if we should ask again in the future (i.e. they
                // did not check 'never ask again')
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    shouldShowCameraPermissionRationale = true;
                } else {
                    // denied & shouldn't ask again. deal with it (•_•) ( •_•)>⌐■-■ (⌐■_■)
                    failEnd();
                }
            }
        }

    }

    @Override protected void onPostResume() {
        super.onPostResume();
        if(shouldShowCameraPermissionRationale){
            showPermissionRationaleDialog();
            shouldShowCameraPermissionRationale = false;
        }
    }

}