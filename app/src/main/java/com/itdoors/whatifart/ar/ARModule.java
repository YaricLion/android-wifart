package com.itdoors.whatifart.ar;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false,
        library = true
)
public final class ARModule {

    @Provides @Singleton TextureService provideTextureService(Application app){
        return new TextureServiceImpl(app);
    }

}
