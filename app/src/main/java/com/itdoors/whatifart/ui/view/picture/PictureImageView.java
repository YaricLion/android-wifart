package com.itdoors.whatifart.ui.view.picture;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.util.Injector;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PictureImageView extends FrameLayout {

    @Bind(R.id.picture_view_scale_image) SubsamplingScaleImageView subsamplingScaleImageView;

    @Inject  Picasso picasso;

    private String url;
    private Picture picture;

    private boolean loading = true;

    public PictureImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Injector.obtain(context.getApplicationContext()).inject(this);
        }
    }

    public void setup(String url){
        this.url = url;
    }

    public void setup(Picture picture){
        this.picture = picture;
    }

    private void fill(){
        if(url != null)
            loadImage(url);
        else if(picture != null)
            loadImage(picture.getImageLink());
        else
            loading = false;
    }

    private Target target = new Target() {
        @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            subsamplingScaleImageView.setImage(ImageSource.bitmap(bitmap));
        }
        @Override public void onBitmapFailed(Drawable errorDrawable) {}
        @Override public void onPrepareLoad(Drawable placeHolderDrawable) {}
    };

    private void loadImage(String url){
        picasso.load(url).placeholder(R.drawable.image_placeholder).into( target );
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        fill();
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(loading) {
            picasso.cancelRequest(target);
        }
    }

}

