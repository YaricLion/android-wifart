package com.itdoors.whatifart.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by v014nd on 05.04.2016.
 */
public class TargetPainterPictureInfo {
    @SerializedName("target")
    private final String uid;
    @SerializedName("info")
    private final PainterPicturesInfo info;

    public TargetPainterPictureInfo(String uid, PainterPicturesInfo info) {
        this.uid = uid;
        this.info = info;
    }

    public String getUid() {
        return uid;
    }

    public PainterPicturesInfo getInfo() {
        return info;
    }
}
