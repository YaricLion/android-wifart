package com.itdoors.whatifart.ui;

import android.app.Application;
import android.content.SharedPreferences;

import com.itdoors.whatifart.ui.activity.ActivityContainer;
import com.itdoors.whatifart.ui.activity.FragmentContainer;
import com.itdoors.whatifart.ui.activity.finlandia.FinlandiaActivity;
import com.itdoors.whatifart.ui.activity.picture.PictureImageActivity;
import com.itdoors.whatifart.ui.activity.painter.PainterImageActivity;
import com.itdoors.whatifart.ui.activity.picture.PictureActivity;
import com.itdoors.whatifart.ui.activity.splash.SplashScreenActivity;
import com.itdoors.whatifart.ui.activity.tutorial.TutorialActivity;
import com.itdoors.whatifart.ui.dialog.AppNeedCameraDialogFragment;
import com.itdoors.whatifart.ui.dialog.AppNeedStorageDialogFragment;
import com.itdoors.whatifart.ui.dialog.screenshot.ScreenShotDialogFragment;
import com.itdoors.whatifart.ui.fragment.PainterImageFragment;
import com.itdoors.whatifart.ui.fragment.PictureFragment;
import com.itdoors.whatifart.ui.fragment.PictureImageFragment;
import com.itdoors.whatifart.ui.view.painter.PainterImageView;
import com.itdoors.whatifart.ui.view.picture.PictureImageView;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.prefs.BooleanPreference;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module(
        injects = {
                PictureActivity.class,
                PictureFragment.class,
                ScreenShotDialogFragment.class,
                AppNeedCameraDialogFragment.class,
                TutorialActivity.class,
                AppNeedStorageDialogFragment.class,

                PictureImageView.class,
                PainterImageView.class,

                PictureImageActivity.class,
                PainterImageActivity.class,

                PictureImageFragment.class,
                PainterImageFragment.class,

                SplashScreenActivity.class,
                FinlandiaActivity.class
        },

        complete = false,
        library = true
)

public final class UiModule {

    @Provides @Singleton ActivityContainer provideAppContainer(){
        return ActivityContainer.DEFAULT;
    }

    @Provides @Singleton FragmentContainer provideFragmentContainer(){
        return FragmentContainer.DEFAULT;
    }

    @Provides @Singleton @Named("io") Scheduler provideSubscribeOnScheduler() {
        return Schedulers.io();
    }

    @Provides @Singleton @Named("main") Scheduler provideObserveOnScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides @Singleton ScreenShotService provideScreenShotService(Application application){
        return new ScreenShotServiceImpl(application);
    }

    @Provides @Singleton Device provideDevice(Application app){
        return new Device(app);
    }

    @Provides @Singleton EventBus provideEventBus(){
        return EventBus.getDefault();
    }

    @Provides @Singleton @Named("isFirstLaunch") BooleanPreference provideFirstLaunchPreference(SharedPreferences preferences){
        return new BooleanPreference(preferences, "isFirstLaunch");
    }


}
