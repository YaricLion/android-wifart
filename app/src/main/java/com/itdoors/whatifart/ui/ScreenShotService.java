package com.itdoors.whatifart.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.util.PictureShareUtils;

import java.io.File;

import rx.Observable;

/**
 * Created by yariclion on 08.03.2016.
 */
public interface ScreenShotService {

    Observable<Intent> share(Bitmap bm, Picture pic);
    Observable<File> save(Bitmap bm, Picture pic);

}
