package com.itdoors.whatifart.ui.activity.picture;

import android.os.Bundle;

import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.ContainerActivity;
import com.itdoors.whatifart.ui.fragment.PictureImageFragment;

public class PictureImageActivity extends ContainerActivity<PictureImageFragment>{

    @Override protected PictureImageFragment setupFragment(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String url = bundle.getString(Intents.Picture.PICTURE_URL);
            if(url != null) {
                return PictureImageFragment.newInstance(url);
            }
            else {
                Picture picture = bundle.getParcelable(Intents.Picture.PICTURE);
                if (picture != null){
                    return PictureImageFragment.newInstance(picture);
                }
            }
        }
        throw new IllegalStateException("Failed PictureImageActivity setup !");
    }

}
