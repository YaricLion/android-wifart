package com.itdoors.whatifart.ui.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.AboutProgramDialogDismissedEvent;
import com.itdoors.whatifart.ui.activity.main.MainARActivity;
import com.itdoors.whatifart.util.Device;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.ObjectGraph;
import de.greenrobot.event.EventBus;

/**
 * Created by v014nd on 22.03.2016.
 */
public class AboutProgramFragment extends DialogFragment {

    @Inject EventBus eventBus;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Override  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ObjectGraph objectGraph = ((MainARActivity)getActivity()).getObjectGraph();
        objectGraph.inject(this);

        View view = inflater.inflate(R.layout.fragment_dialog_about_program, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.view_about_program_app_itdoors_site_link) public void onSiteClicked(View v){
        String url = getResources().getString(R.string.app_itdoors_site_link);
        Device.startBrowserLink(getActivity(), url);
    }

    @Override  public void onDismiss(DialogInterface dialog) {
        eventBus.post(new AboutProgramDialogDismissedEvent());
        super.onDismiss(dialog);
    }

}
