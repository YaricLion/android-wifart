package com.itdoors.whatifart.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.itdoors.whatifart.data.model.Picture;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Random;

import timber.log.Timber;

/**
 * Created by yariclion on 30.03.2015.
 */
public final class PictureShareUtils {

    private PictureShareUtils(){
        throw new AssertionError("No instances.");
    }

    private static final Random random = new Random(System.currentTimeMillis());

    public static class FailToSaveException extends IOException {

        private final Picture picture;

        public FailToSaveException(Picture picture){
            this(picture, null, null);
        }

        public FailToSaveException(Picture picture, String msg){
           this(picture, msg, null);
        }

        public FailToSaveException(Picture picture, String message, Throwable cause) {
            super(message, cause);
            this.picture = picture;
        }

        public Picture getPicture() {
            return picture;
        }
    }

    public static String savePictureToMedia(Context context, Picture picture, Bitmap bitmap){

        ContentResolver cr = context.getContentResolver();
        String name = getName(picture);
        String path = MediaStore.Images.Media.insertImage(cr, bitmap, name, null);
        Timber.i("Save image %s to %s", name, path);

        if(path == null)
            Timber.e("Failed to save image %s.", name);

        return path;
    }

    private static String getName(Picture picture){

        String now = CalendarUtils.now("yyyyMMdd");
        String md5 = MD5.md5(picture.getName() + random.nextLong());
        String name = "WIF_ART_IMG" + "_" + now + "_" + md5;
        return name;

    }

    public static File saveImageToPictures(Context context, String appDir, Picture picture, Bitmap bitmap) throws FailToSaveException{

        File dirToSave = createDirectoryForPicturesIfNeed(appDir);

        Timber.d("saveImageToPictures : dirToSave = " + dirToSave);
        String name = getName(picture);
        Timber.d("saveImageToPictures : name = " + name);

        File picFile = new File(dirToSave, name + ".jpeg");

        //Clean up before execution
        if(picFile.exists()) {
            Timber.e("saveImageToPictures : existing file = " + picFile.toString());
            picFile.delete();
        }
        OutputStream out = null;
        try {

            out = new BufferedOutputStream( new FileOutputStream(picFile));
            boolean ok = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            if(!ok)
                throw new FailToSaveException(picture, "Fail to save picture : " + picture.toString() +" to file :" + picFile.getAbsolutePath() +".\n"
                        + "Fail to compress bitmap !");
            else {
                Timber.e("saveImageToPictures : result file = " + picFile.toString());
                return picFile;
            }
        }
        catch (IOException ex){
            Timber.e(ex, "saveImageToPictures : fail ");
            //Clean up after exception
            if(picFile.exists())
                picFile.delete();

            throw new FailToSaveException(picture,"Fail to save picture : " + picture.toString() +" to file :" + picFile.getAbsolutePath() , ex);
        }
        finally {
            if(out != null){
                try{
                    out.close();
                } catch (IOException ignore) {}
            }
        }
    }

    public static Uri addToContentResolver(Context context, File picture, String name){

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, name);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis ());
        values.put(MediaStore.Images.ImageColumns.BUCKET_ID, picture.toString().toLowerCase(Locale.US).hashCode());
        values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, picture.getName().toLowerCase(Locale.US));
        values.put("_data", picture.getAbsolutePath());

        Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        return uri;
    }

    private static File createDirectoryForPicturesIfNeed (String dirName){
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), dirName);
        if (!dir.exists ()){
            dir.mkdirs();
        }
        return dir;
    }

    public static Intent getSharePictureIntent(File file){
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        return share;
    }

}
