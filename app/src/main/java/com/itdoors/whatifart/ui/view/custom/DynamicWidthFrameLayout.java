package com.itdoors.whatifart.ui.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by yariclion on 11.03.2016.
 */
public class DynamicWidthFrameLayout extends FrameLayout{

    private double mWeightRatio;

    public DynamicWidthFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setWeightRatio(double ratio) {
        if (ratio != mWeightRatio) {
            mWeightRatio = ratio;
            requestLayout();
        }
    }

    public double getWeightRatio() {
        return mWeightRatio;
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mWeightRatio > 0.0) {
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int width = (int) (height * mWeightRatio);
            setMeasuredDimension(width, height);
        }
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
