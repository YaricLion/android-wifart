package com.itdoors.whatifart.ui.activity.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.itdoors.whatifart.event.AboutProgramDialogDismissedEvent;
import com.itdoors.whatifart.event.AboutWifDialogDismissedEvent;
import com.itdoors.whatifart.event.permission.AllowCameraEvent;
import com.itdoors.whatifart.event.permission.DenyCameraEvent;
import com.itdoors.whatifart.event.permission.ShowStorageRationaleEvent;
import com.itdoors.whatifart.ui.Intents;
import com.itdoors.whatifart.ui.activity.BaseActivity;
import com.itdoors.whatifart.ui.activity.finlandia.FinlandiaActivity;
import com.itdoors.whatifart.ui.activity.picture.PictureActivity;
import com.itdoors.whatifart.ui.activity.tutorial.TutorialActivity;
import com.itdoors.whatifart.ui.activity.wif.AboutWIFActivity;
import com.itdoors.whatifart.ui.dialog.AboutProgramFragment;
import com.itdoors.whatifart.ui.dialog.AppNeedCameraDialogFragment;
import com.itdoors.whatifart.ui.dialog.AppNeedStorageDialogFragment;
import com.itdoors.whatifart.ui.dialog.FinishOnErrorDialogFragment;
import com.itdoors.whatifart.ui.dialog.MessageDialogFragment;
import com.itdoors.whatifart.ui.dialog.VersionFragment;
import com.itdoors.whatifart.ui.gl.PainterInfoRenderer;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.ui.dialog.screenshot.ScreenShotDialogFragment;
import com.itdoors.whatifart.data.api.PainterImageService;
import com.itdoors.whatifart.ar.TextureService;
import com.itdoors.whatifart.data.model.PainterPicturesInfo;
import com.itdoors.whatifart.event.ScreenShotReadyEvent;
import com.itdoors.whatifart.event.TakeScreenShotEvent;
import com.itdoors.whatifart.ui.gl.Texture;
import com.itdoors.whatifart.util.AnimationHelper;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.Injector;
import com.itdoors.whatifart.util.ToastUtils;
import com.itdoors.whatifart.util.ViewSubscriptions;
import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.DataSet;
import com.qualcomm.vuforia.ObjectTracker;
import com.qualcomm.vuforia.STORAGE_TYPE;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.TargetFinder;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;
import com.qualcomm.vuforia.Type;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationControl;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationException;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationSession;
import com.qualcomm.vuforia.samples.SampleApplication.utils.SampleApplicationGLView;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.ObjectGraph;
import de.greenrobot.event.EventBus;
import timber.log.Timber;


public class MainARActivity extends BaseActivity implements SampleApplicationControl {

    private static final int PERMISSIONS_REQUEST_CAMERA  = 0;

    // Stores the current status of the target ( if is being displayed or not )
    private static final int PainterInfoINFO_NOT_DISPLAYED = 0;
    private static final int PainterInfoINFO_IS_DISPLAYED = 1;

    private static final int DELAY_FOR_HIDING_ACTION_BAR = 5000;

    private int mPainterInfoInfoStatus = PainterInfoINFO_NOT_DISPLAYED;

    private SampleApplicationSession vuforiaAppSession;
    private PainterPicturesInfo mPainterInfoData;

    // Indicates if the app is currently loading the PainterInfo data
    private boolean mIsLoadingPainterInfoData = false;

    // Helpers to detect events such as double tapping:
    private GestureDetector mGestureDetector = null;
    private GestureDetector.SimpleOnGestureListener mSimpleListener = null;

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    // Our OpenGL view:
    private SampleApplicationGLView mGlView;

    // Our renderer:
    private PainterInfoRenderer mRenderer;

    //Lock to protect dataSet, becouse dataSet object is shared between main and async threads
    private final Object dataSetLock = new Object();
    private DataSet mCurrentDataset;

    private final String DATABASE_NAME = "wifart.xml";

    @Bind(R.id.ar_camera_overlay)    RelativeLayout mUILayout;
    @Bind(R.id.ar_loading_layout)    View mLoadingDialogContainer;
    @Bind(R.id.ar_close_button)      View mCloseButton;
    @Bind(R.id.ar_more_button)       View mMoreButton;
    @Bind(R.id.ar_flash_button)      View mFlashButton;
    @Bind(R.id.ar_screenshot_button) View mScreenShotButton;

    //private ImageView mTestImageView;

    private final Object targetIdLock = new Object();
    private String lastTargetId = null;

    private boolean mFlash = false;

    @Inject EventBus eventBus;
    @Inject PainterImageService imageService;
    @Inject @Named("activity_onPauseSubscription") ViewSubscriptions subscriptions;
    @Inject TextureService textureService;

    private ObjectGraph activityGraph;

    boolean mIsDroidDevice = false;

    private Runnable hideActionBarIfShowing = () -> {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && actionBar.isShowing())
            actionBar.hide();
    };

    //Camera
    private boolean shouldShowCameraPermissionRationale = false;
    private boolean shouldInitAR = false;

    //Storage
    private boolean shouldShowStoragePermissionRationale = false;
    private int requestedStorageCode = ScreenShotDialogFragment.ACTION_NONE;

    //InitializationError
    private boolean shouldShowInitializationErrorDialog = false;
    private String initializationErrorMsg = null;

    @Override protected void onPostResume() {
        super.onPostResume();

        if(shouldShowCameraPermissionRationale){
            showCameraPermissionRationaleDialog();
            shouldShowCameraPermissionRationale = false;
        }
        else if(shouldInitAR){
            shouldInitAR = false;
            initAR();
        }

        if(shouldShowStoragePermissionRationale){
            showStoragePermissionRationaleDialog();
            shouldShowStoragePermissionRationale = false;
        }

        if(shouldShowInitializationErrorDialog){
            showInitializaionErrorDialog(initializationErrorMsg);
            shouldShowInitializationErrorDialog = false;
            initializationErrorMsg = null;
        }
    }

    private void requestCamera(){
        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
    }

    private boolean hasCameraPermission(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public void onEventMainThread(AllowCameraEvent event){
        requestCamera();
    }

    public void onEventMainThread(DenyCameraEvent event){
        failEnd();
    }

    public void onEventMainThread(ShowStorageRationaleEvent event){
        shouldShowStoragePermissionRationale = true;
        requestedStorageCode = event.getCode();
    }

    private void showCameraPermissionRationaleDialog(){
        AppNeedCameraDialogFragment fragment = new AppNeedCameraDialogFragment();
        fragment.show(getSupportFragmentManager(), "cameraRationale");
    }

    private void showInitializaionErrorDialog(String message){
        MessageDialogFragment fragment = FinishOnErrorDialogFragment.newInstance(getString(R.string.INIT_ERROR), message);
        fragment.show(getSupportFragmentManager(), "finishInitError");
    }

    private void showStoragePermissionRationaleDialog(){
        AppNeedStorageDialogFragment fragment = AppNeedStorageDialogFragment.newInstance(requestedStorageCode);
        fragment.show(getSupportFragmentManager(), "storageRationale");
        requestedStorageCode = ScreenShotDialogFragment.ACTION_NONE;
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                shouldInitAR = true;
            } else {
                // if permission was denied check if we should ask again in the future (i.e. they
                // did not check 'never ask again')
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    shouldShowCameraPermissionRationale = true;
                } else {
                    // denied & shouldn't ask again. deal with it (•_•) ( •_•)>⌐■-■ (⌐■_■)
                    failEnd();
                }
            }
        } else super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private void failEnd(){
        ToastUtils.ToastLong(getApplicationContext(), getString(R.string.permission_tutorial_rationale_dialog_denied_msg));
        finish();
    }

    @Override protected void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override protected void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    public void deinitPainterInfos() {
        // Get the object tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
        {
            Timber.e("Failed to destroy the tracking data set because the ObjectTracker has not been initialized.");
            return;
        }

        // Deinitialize target finder:
        TargetFinder finder = objectTracker.getTargetFinder();
        finder.deinit();
    }


    private void initStateVariables() {
        mRenderer.setRenderState(PainterInfoRenderer.RS_SCANNING);
        mRenderer.setProductTexture(null);
        cleanTargetTrackedId();
    }

    /**
     * Function to generate the OpenGL Texture Object in the renderFrame thread
     */

    private void productTextureIsCreated() {
        Timber.d("Product texture created!");
        mRenderer.setRenderState(PainterInfoRenderer.RS_TEXTURE_GENERATED);
    }

    /** Cleans the lastTargetTrackerId variable */
    private void cleanTargetTrackedId() {
        synchronized (targetIdLock) {
            lastTargetId = null;
        }
    }

    // Called when the activity first starts or needs to be recreated after
    // resuming the application or a configuration change.
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Timber.d("onCreate");

        ObjectGraph appGraph = Injector.obtain(getApplication());
        activityGraph = appGraph.plus(new ARPicturePainterModule(this));
        activityGraph.inject(this);

        initViews();
        setupActionBar();

        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");

        if (hasCameraPermission()){
            initAR();
        } else {
            requestCamera();
        }

    }

    private void initAR(){

        vuforiaAppSession = new SampleApplicationSession(this);

/*
        // Use an OrientationChangeListener here to capture all orientation changes.  Android
        // will not send an Activity.onConfigurationChanged() callback on a 180 degree rotation,
        // ie: Left Landscape to Right Landscape.  Vuforia needs to react to this change and the
        // SampleApplicationSession needs to update the Projection Matrix.
        OrientationEventListener orientationEventListener = new OrientationEventListener(this) {
            @Override public void onOrientationChanged(int i) {
                int activityRotation = getWindowManager().getDefaultDisplay().getRotation();
                if(mLastRotation != activityRotation)
                {
                    // Signal the ApplicationSession to refresh the projection matrix
                    vuforiaAppSession.setProjectionMatrix();
                    mLastRotation = activityRotation;
                }
            }

            int mLastRotation = -1;
        };

        int orientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR;
        if(orientationEventListener.canDetectOrientation())
            orientationEventListener.enable();
        else // If we can't use the above orientation change listener, disable auto-rotate
            orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        //vuforiaAppSession.initAR(this, orientation);*/

        vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        // Create the gesture detector that will handle the single and double taps:
        mSimpleListener = new GestureDetector.SimpleOnGestureListener();
        mGestureDetector = new GestureDetector(getApplicationContext(), mSimpleListener);

        // Set the double tap listener:
        mGestureDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener(){

            public boolean onDoubleTap(MotionEvent e){
                // We do not react to this event
                return false;
            }

            public boolean onDoubleTapEvent(MotionEvent e){
                // We do not react to this event
                return false;
            }

            // Handle the single tap
            public boolean onSingleTapConfirmed(MotionEvent e){
                if (mPainterInfoInfoStatus == PainterInfoINFO_NOT_DISPLAYED){
                    // Calls the Autofocus Method
                    boolean result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);
                    if (!result)
                        Timber.e("SingleTapUp. Unable to trigger focus");
                    // If the PainterInfo info is displayed it shows the PainterInfo data web view
                    //ToastUtils.ToastShort(getApplicationContext(), "Single tap : autofocus -" + result);
                }
                toggleActionBar();
                return true;
            }
        });

    }

    private void showTutorial(){
        Timber.d("First launch setup");
        Intent intent = new Intent(this, TutorialActivity.class);
        intent.putExtra(Intents.Tutorial.FIRST_LAUNCH, false);
        startActivity(intent);
    }

    private void setupActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            if(actionBar.isShowing()){
                mainHandler.postDelayed(hideActionBarIfShowing , DELAY_FOR_HIDING_ACTION_BAR);
            }
        }
    }

    // Called when the activity will start interacting with the user.
    @Override protected void onResume() {
        Timber.d("onResume");
        super.onResume();

        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        try {
            if(vuforiaAppSession != null)
                vuforiaAppSession.resumeAR();
        } catch (SampleApplicationException e) {
            Timber.e(e, "SampleApplicationException : onResume on resumeAR");
        }

        // Resume the GL view:
        if (mGlView != null) {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }

        mPainterInfoInfoStatus = PainterInfoINFO_NOT_DISPLAYED;

        // By default the 2D Overlay is hidden
        hideButtons();
    }


    // Callback for configuration changes the activity handles itself
    @Override public void onConfigurationChanged(Configuration config) {
        Timber.d("onConfigurationChanged");
        super.onConfigurationChanged(config);

        if(vuforiaAppSession != null)
            vuforiaAppSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override protected void onPause() {
        Timber.d("onPause");
        super.onPause();

        try {

            if(vuforiaAppSession != null)
                vuforiaAppSession.pauseAR();

        } catch (SampleApplicationException e) {
            Timber.e(e.getString());
        }

        // When the camera stops it clears the Product Texture ID so next time
        // textures
        // Are recreated
        if (mRenderer != null) {
            mRenderer.deleteCurrentProductTexture();

            // Initialize all state Variables
            initStateVariables();
        }

        // Pauses the OpenGLView
        if (mGlView != null) {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        subscriptions.unsubscribe();
    }


    // The final call you receive before your activity is destroyed.
    @Override protected void onDestroy() {
        Timber.d( "onDestroy");

        activityGraph = null;
        super.onDestroy();

        try {

            if(vuforiaAppSession != null)
                vuforiaAppSession.stopAR();

        } catch (SampleApplicationException e) {
            Timber.e(e, "Fail to stop AR !");
        }

        System.gc();
    }


    private void initViews() {

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //mTestImageView = (ImageView)mUILayout.findViewById(R.id.ar_test_bitmap_view);
        // Shows the loading indicator at start
        showLoading();

        // Sets the Close Button functionality
        mCloseButton.setOnClickListener(v -> {
            AnimationHelper.pulse(v, () -> {
                close();
            });
        });

        mScreenShotButton.setOnClickListener(v -> {
            AnimationHelper.pulse(v, () -> {
                takeScreenShot();
            });
        });
        // Sets the Close Button functionality
        mMoreButton.setOnClickListener(v -> {
            AnimationHelper.pulse(v, () -> {
                moreInfo();
            });
        });

        // Sets the Close Button functionality
        mFlashButton.setOnClickListener(new FlashClickListener());
        hideButtons();
    }

    private class FlashClickListener implements OnClickListener {

        boolean on = false;
        public void onClick(View v) {
            AnimationHelper.pulse(v, () -> {
                flash(FlashClickListener.this);
            });

        }
        public void switchMode(){
            if (on) {
                ((ImageView)mFlashButton).setImageResource(R.drawable.ic_on);
                on = false;
            } else {
                ((ImageView)mFlashButton).setImageResource(R.drawable.ic_off);
                on = true;
            }
        }
    }

    private void close(){
        // Updates application status
        mPainterInfoInfoStatus = PainterInfoINFO_NOT_DISPLAYED;

        hideLoading();

        // Checks if the app is currently loading a PainterInfo data
        if (mIsLoadingPainterInfoData){

            mIsLoadingPainterInfoData = false;
            cleanTargetTrackedId();

        }

        enterScanningMode();
    }

    private void takeScreenShot(){
        eventBus.post(new TakeScreenShotEvent());
    }
    private void moreInfo(){
        synchronized (targetIdLock) {
            if (lastTargetId != null) {
                if(!lastTargetId.contains(FinlandiaActivity.FINLANDIA)) {
                    Intent intent = new Intent(MainARActivity.this, PictureActivity.class);
                    intent.putExtra(Intents.Picture.PICTURE_ID, lastTargetId);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MainARActivity.this, FinlandiaActivity.class);
                    intent.putExtra(Intents.Picture.PICTURE_ID, lastTargetId);
                    startActivity(intent);
                }
            }
        }
    }

    private void flash(FlashClickListener listener){
        boolean ok = CameraDevice.getInstance().setFlashTorchMode(!mFlash);
        if (ok){
            mFlash = !mFlash;
            listener.switchMode();
        } else {
            String msg = getString(mFlash ? R.string.menu_flash_error_off : R.string.menu_flash_error_on);
            ToastUtils.ToastShort(getApplicationContext(), msg);
            Timber.e(msg);
        }
    }

    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        // Initialize the GLView with proper flags
        mGlView = new SampleApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);

        // Setups the Renderer of the GLView
        mRenderer = new PainterInfoRenderer(vuforiaAppSession, this);
        mGlView.setRenderer(mRenderer);

        initStateVariables();
    }

    public boolean isCurrentTarget(TrackableResult trackable){
        synchronized (targetIdLock) {
            return trackable.getTrackable().getName().equals(lastTargetId);
        }
    }

    //Should be called on MainThread
    public void updateRendering(TrackableResult newTrackable) {

        Device.throwIfNotMainThread();
        synchronized (targetIdLock) {

            if (newTrackable != null) {

                boolean sameTrackable = true;
                if (!newTrackable.getTrackable().getName().equals(lastTargetId)) {

                    mRenderer.deleteCurrentProductTexture();
                    createProductTexture(newTrackable);
                    sameTrackable = false;

                } else {

                    mRenderer.setRenderState(PainterInfoRenderer.RS_NORMAL);

                }

                mRenderer.setFramesToSkipBeforeRenderingTransition(PainterInfoRenderer.MAX_BEFORE_RENDERING_TRANSITION);
                lastTargetId = newTrackable.getTrackable().getName();

                enterContentMode();
                if (sameTrackable) {
                    showButtons();
                }
            } else
                Timber.e("Failed to create new trackable.");

        }
    }

    /**
     * Starts application content Mode Displays UI OVerlays and turns Cloud
     * Recognition off
     */
    private void enterContentMode() {

        Device.throwIfNotMainThread();
        // Updates state variables
        mPainterInfoInfoStatus = PainterInfoINFO_IS_DISPLAYED;

        // Enters content mode to disable Cloud Recognition
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
                .getTracker(ObjectTracker.getClassType());
        TargetFinder targetFinder = objectTracker.getTargetFinder();

        // Stop Cloud Recognition
        targetFinder.stop();

    }

    /**
     * Generates a texture for the PainterInfo data fetching the PainterInfo info from the uid
     */
    private void createProductTexture(TrackableResult result) {

        Device.throwIfNotMainThread();
        Trackable trackable = result.getTrackable();

        String userData = (String) trackable.getUserData();

        int id = trackable.getId();
        String name = trackable.getName();
        Object data = trackable.getUserData();
        Type type = trackable.getType();

        Timber.d( "printUserData : trackable = " + "{" + "id:" + id + ", " + "name:" + name + ", " + "data:" + data + "type:" + type + "}");
        Timber.d("UserData:Retreived User Data	\"" + userData + "\"");

        final String uid = name;
        Timber.d("Create product texture start !");
        subscriptions.add(

                imageService.getPainterPicturesInfo(uid)
                        .doOnNext(painterResponse -> {
                            mainHandler.post(() -> {
                                mPainterInfoData = painterResponse.getInfo();
                            });
                        })
                        .flatMap(textureService::createTexture),

                pair -> {

                    Texture texture = pair.first;
                    //Bitmap bitmap = pair.second;
                    //mTestImageView.setImageBitmap(bitmap);

                    Timber.d("Texture. Create product texture end !");
                    Timber.d("Texture. Created : " + texture.getWidth() + ";" + texture.getHeight());

                    // Hides the loading dialog from a UI thread
                    hideLoading();
                    mRenderer.setProductTexture(texture);
                    mIsLoadingPainterInfoData = false;

                    productTextureIsCreated();
                    showButtons();
                },
                throwable -> {
                    Timber.e(throwable, "Texture fail!");
                }

        );
    }


    /** Hides the 2D Overlay view and starts C service again */
    private void enterScanningMode() {
        // Hides the 2D Overlay
        hideButtons();

        // Enables Cloud Recognition Scanning Mode
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
                .getTracker(ObjectTracker.getClassType());
        TargetFinder targetFinder = objectTracker.getTargetFinder();

        // Start Cloud Recognition
        targetFinder.startRecognition();

        // Clear all trackables created previously:
        targetFinder.clearTrackables();

        // Updates state variables
        mRenderer.setRenderState(PainterInfoRenderer.RS_SCANNING);
    }


    private void showButtons() {
        mCloseButton.setVisibility(View.VISIBLE);
        mMoreButton.setVisibility(View.VISIBLE);
        mScreenShotButton.setVisibility(View.VISIBLE);
    }

    private void hideButtons() {
        mCloseButton.setVisibility(View.GONE);
        mMoreButton.setVisibility(View.GONE);
        mScreenShotButton.setVisibility(View.GONE);
    }

    private void showLoading() {
        mLoadingDialogContainer.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        mLoadingDialogContainer.setVisibility(View.GONE);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    @Override public boolean doLoadTrackersData() {
        ////////////////////////////////////////////////////////////////////////////////////////////
        //Changing logic to device database case

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        synchronized (dataSetLock) {

            if (mCurrentDataset == null)
                mCurrentDataset = objectTracker.createDataSet();

            if (mCurrentDataset == null)
                return false;

            if (!mCurrentDataset.load(DATABASE_NAME, STORAGE_TYPE.STORAGE_APPRESOURCE))
                return false;

            if (!objectTracker.activateDataSet(mCurrentDataset))
                return false;

            int numTrackables = mCurrentDataset.getNumTrackables();
            for (int count = 0; count < numTrackables; count++) {

                Trackable trackable = mCurrentDataset.getTrackable(count);

               /* if(isExtendedTrackingActive())
                {
                    trackable.startExtendedTracking();
                }*/

                //String name = "Current Dataset : " + trackable.getName();
                String name = trackable.getName();
                trackable.setUserData(name);
                Timber.d( "UserData: " + trackable.getUserData());

            }

        }

        return true;
    }


    @Override public boolean doUnloadTrackersData() {
        //return true;
        ////////////////////////////////////////////////////////////////////////////////////////////

        //Changing logic to device database case
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        synchronized (dataSetLock) {

            if (mCurrentDataset != null && mCurrentDataset.isActive()) {
                if (objectTracker.getActiveDataSet().equals(mCurrentDataset)
                        && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                    result = false;
                } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                    result = false;
                }

                mCurrentDataset = null;
            }

        }
        return result;
    }


    @Override  public void onInitARDone(SampleApplicationException exception) {

        if (exception == null) {

            initApplicationAR();

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            // Start the camera:
            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA.CAMERA_DEFAULT);
            } catch (SampleApplicationException e) {
                Timber.e(e, "Fail to start camera !");
            }

            mRenderer.setActive(true);

            boolean result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);
            if (!result)
                Timber.e("Unable to enable continuous autofocus");

            mUILayout.bringToFront();
            mUILayout.setBackgroundColor(Color.TRANSPARENT);
            hideLoading();

            ToastUtils.ToastShort(getApplicationContext(), getString(R.string.ar_single_tap_to_autofocus));

        } else {
            Timber.e(exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }


    // Shows initialization error messages as System dialogs
    private void showInitializationErrorMessage(String message) {
        mainHandler.post(() -> {
            shouldShowInitializationErrorDialog = true;
            initializationErrorMsg = message;
        });
    }

    @Override public void onQCARUpdate(State state) {
        ////////////////////////////////////////////////////////////////////////////////////////////
        //Change logic to device database case
        //Empty
    }

    @Override public boolean doInitTrackers() {

        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Timber.e("Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Timber.i( "Tracker successfully initialized");
        }

        return result;
    }


    @Override public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        // Start the tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
                .getTracker(ObjectTracker.getClassType());
        objectTracker.start();

        return result;
    }


    @Override public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
                .getTracker(ObjectTracker.getClassType());
        if(objectTracker != null)
            objectTracker.stop();
        else
            result = false;

        return result;
    }


    @Override public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;
        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());
        return result;
    }

    public void onEventMainThread(TakeScreenShotEvent event){
        if(mRenderer != null){
            mRenderer.takeScreenShot();
        }
    }

    public void onEventMainThread(ScreenShotReadyEvent event){
        if(mPainterInfoData != null) {
            Bitmap bm = event.getBitmap();
            ScreenShotDialogFragment fragment = ScreenShotDialogFragment.newInstance(bm, mPainterInfoData.getPicture());
            fragment.show(getSupportFragmentManager(), "ScreenShot");
        }
    }

    public void onEventMainThread(AboutWifDialogDismissedEvent event){
        showFlash();
    }

    public void onEventMainThread(AboutProgramDialogDismissedEvent event){
        showFlash();
    }

    private void showFlash(){
        mFlashButton.setVisibility(View.VISIBLE);
    }
    private void hideFlash(){
        mFlashButton.setVisibility(View.INVISIBLE);
    }

    @Override public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return activityGraph;
        }
        return super.getSystemService(name);
    }

    public ObjectGraph getObjectGraph(){
        return activityGraph;
    }

    private void showAboutWIF(){
        //AboutWIFFragment fragment = new AboutWIFFragment();
        //fragment.show(getSupportFragmentManager(), "AboutWif");
        Intent intent = new Intent(this, AboutWIFActivity.class);
        startActivity(intent);
    }

    private void showAboutProgram(){
        AboutProgramFragment fragment = new AboutProgramFragment();
        fragment.show(getSupportFragmentManager(), "AboutProgram");
    }

    private void openInMarket(){
        Device.openAppInMarket(this, getPackageName());
    }

    private void showVersion(){
        VersionFragment fragment = new VersionFragment();
        fragment.show(getSupportFragmentManager(), "Version");
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_ar, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.main_menu_action_about_wif:
                //hideFlash();
                showAboutWIF();
                toggleActionBar();
                return true;

            case R.id.main_menu_action_about_program:
                hideFlash();
                showAboutProgram();
                toggleActionBar();
                return true;

            case R.id.main_menu_action_instruction:
                showTutorial();
                return true;

            case R.id.main_menu_action_open_in_market:
                openInMarket();
                toggleActionBar();
                return true;

            case R.id.main_menu_action_version:
                showVersion();
                toggleActionBar();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void toggleActionBar(){

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (actionBar.isShowing()) {
                mainHandler.removeCallbacks(hideActionBarIfShowing);
                actionBar.hide();
            }
            else {
                mainHandler.removeCallbacks(hideActionBarIfShowing);
                actionBar.show();
                mainHandler.postDelayed(hideActionBarIfShowing, DELAY_FOR_HIDING_ACTION_BAR);
            
            }
        }

    }

}

