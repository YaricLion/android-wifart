package com.itdoors.whatifart.event;

import com.itdoors.whatifart.data.model.Picture;

public final class ShowPictureImageEvent {
    private final Picture picture;
    public ShowPictureImageEvent(Picture picture) {
        this.picture = picture;
    }
    public Picture getPicture() {
        return picture;
    }
}
