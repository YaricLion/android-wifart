package com.itdoors.whatifart.ui.view.picture;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.common.base.Optional;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.api.PainterService;
import com.itdoors.whatifart.data.model.Painter;
import com.itdoors.whatifart.data.model.PainterPicturesInfo;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.event.ShowPainterImageEvent;
import com.itdoors.whatifart.event.ShowPictureImageEvent;
import com.itdoors.whatifart.ui.view.custom.BetterViewAnimator;
import com.itdoors.whatifart.ui.view.custom.DynamicWidthImageView;
import com.itdoors.whatifart.util.AnimationHelper;
import com.itdoors.whatifart.util.CircleStrokeTransformation;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.Injector;
import com.itdoors.whatifart.util.ShareHelper;
import com.itdoors.whatifart.util.ViewSubscriptions;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.functions.Action1;
import timber.log.Timber;

public class PictureView extends FrameLayout {

    private final String POPARADA = "poparada";

    @Bind(R.id.picture_detail_animator) BetterViewAnimator animatorView;
    @Bind(R.id.picture_detail_retry)    Button retryBtn;
    @Bind(R.id.picture_detail_image)    ImageView mImageView;

    @Bind(R.id.picture_detail_painter_image)  ImageView mPainterImageView;
    @Bind(R.id.picture_detail_painter_name)   TextView  mPainterNameView;

    @Bind(R.id.picture_detail_technique)     TextView techniqueView;
    @Bind(R.id.picture_detail_year)          TextView yearView;
    @Bind(R.id.picture_detail_sizes)         TextView sizesView;
    //@Bind(R.id.picture_detail_description)   TextView descriptionView;

    @Bind(R.id.picture_detail_picture_description)   TextView pictureDescriptionView;
    @Bind(R.id.picture_detail_painter_description)   TextView painterDescriptionView;
    @Bind(R.id.picture_detail_price)  TextView picturePriceView;

    @Bind(R.id.picture_detail_painter_site)  TextView painterSiteView;

    @Bind(R.id.picture_detail_painter_container) View painterHolderView;
    @Bind(R.id.picture_detail_painter_panel)     View painterHolderPanel;

    @Bind(R.id.picture_detail_title_description) TextView picturenameView;

    @Bind(R.id.action_picture_detail_share)              ImageView pictureShareView;
    @Bind(R.id.action_picture_detail_painter_facebook)   ImageView painterFacebookView;

    @Bind(R.id.action_picture_detail_back) View backBtn;

    @BindString(R.string.currency_grn) String GRN;
    @BindString(R.string.without_name) String withoutName;

    @Inject PainterService service;
    @Inject Device device;
    @Inject Picasso picasso;
    @Inject EventBus eventBus;

    @Inject @Named("PictureViewSubscription")     ViewSubscriptions subscriptions;
    @Inject UrlValidator urlValidator;

    private final CircleStrokeTransformation transformation;

    private Picture picture;
    private Painter painter;
    private String uid;

    private boolean needToLoad = false;

    public PictureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Injector.obtain(context).inject(this);
        }
        transformation =  new CircleStrokeTransformation(context, getResources().getColor(android.R.color.transparent), 0);
    }

    public Action1<PainterPicturesInfo> loadFinishAction = response -> {

        Optional<PainterPicturesInfo> item = Optional.fromNullable(response);
        Optional<Picture> picture = Optional.fromNullable(item.isPresent() ? item.get().getPicture() : null);
        Optional<Painter> painter = Optional.fromNullable(item.isPresent()? item.get().getPainter(): null);

        if(picture.isPresent() && painter.isPresent()){
            setup(picture.get(), painter.get());
            fill(picture.get(), painter.get());
            animatorView.setDisplayedChildId(R.id.picture_detail_content);
        }
        else{
            animatorView.setDisplayedChildId(R.id.picture_detail_error);
        }
    };

    private Action1<Throwable> loadErrorAction = throwable -> {
        Timber.e(throwable, "FAIL TO SHOW PICTURE");
        animatorView.setDisplayedChildId(R.id.picture_detail_error);
    };

    private void load(){
        Timber.d("load()");
        subscriptions.add(getInfo(), loadFinishAction, loadErrorAction);
    }

    private void retry(){
        animatorView.setDisplayedChildId(R.id.picture_detail_progress);
        load();
    }

    private Observable<PainterPicturesInfo> getInfo(){
        return service.getPainterInfo(uid);
    }

    public void setup(Picture picture, Painter painter) {
        if(picture != null && painter != null) {
            needToLoad = false;
            this.uid = String.valueOf(picture.getId());
            this.picture = picture;
            this.painter = painter;
        }
    }

    public void setup(String pictureId) {
        if(pictureId != null) {
            needToLoad = true;
            this.uid = pictureId;
        }
    }

    public Picture getPictureInside(){
        return this.picture;
    }

    public Painter getPainterInside(){
        return this.painter;
    }

    public String getPictureIdInside(){
        return this.uid;
    }
    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        AppCompatActivity activity = (AppCompatActivity) getContext();
        activity.setTitle(null);
    }

    @OnClick(R.id.action_picture_detail_back) void onBackPressed(View v){
        AnimationHelper.pulse(v, () -> {
            AppCompatActivity activity = (AppCompatActivity) getContext();
            activity.finish();
        });
    }

    @OnClick(R.id.picture_detail_retry) void onRetryPressed(View v){
        AnimationHelper.pulse(v, () -> {
            retry();
        });
    }

    private float getRatio(Picture picture) {

        float ratio = 1f;
        if(picture.getRealH() > 0 && picture.getRealW() > 0)
                ratio = (float) picture.getRealW() / (float) picture.getRealH();

        DisplayMetrics metrics = device.getMetrics();
        float W = metrics.widthPixels, H = metrics.heightPixels;

        //if w >> h or w << h
        if (ratio > 1f || ratio < 1f / 2)
            ratio = 1;

        //if screen is to "square" then using half of screen width
        if( H * ratio >  0.65f * W )
            ratio = 1f/2 * W / H;

        return ratio;

    }

    private void fill(Picture picture, Painter painter){

        if(picture != null && painter != null) {

            float ratio = getRatio(picture);
            ((DynamicWidthImageView) mImageView).setWeightRatio(ratio);

            String pictureLink = picture.getImageLink();

            boolean poparada = !StringUtils.isEmpty(pictureLink) && pictureLink.contains(POPARADA);
            if(!poparada) {
                picasso.load(pictureLink)
                        .error(R.drawable.image_placeholder)
                        .fit()
                        .centerInside()
                        .into(mImageView);
            }
            else{
                mImageView.setImageResource(R.drawable.poparada_white);
            }

            Optional<String> pictureName = Optional.fromNullable(picture.getName());
            String pictureNameInfo = pictureName.or(withoutName);
            picturenameView.setText(pictureNameInfo);

            String priceInfo = picture.getPrice() > 0 ?  picture.getPrice() + " " + GRN : null;
            showIfNotEmptyOrGone(picturePriceView, priceInfo);

            String painterLink = painter.getImageLink();
            picasso.load(painterLink)
                        .placeholder(R.drawable.contact_icon)
                        .error(R.drawable.contact_icon)
                        .fit()
                        .centerCrop()
                        .transform(transformation)
                        .into(mPainterImageView);

            Optional<String> painterName = Optional.fromNullable(painter.getName());
            Optional<String> painterSurname = Optional.fromNullable(painter.getSurname());

            StringBuilder nameBuilder = new StringBuilder();
            if(painterName.isPresent())
                nameBuilder.append(painterName.get());
            if(painterSurname.isPresent())
                nameBuilder.append(" ").append(painterSurname.get());

            String name = nameBuilder.toString();
            mPainterNameView.setText(name);

            Optional<String> painterDescription = Optional.fromNullable(painter.getDescription());
            Optional<String> pictureDescription = Optional.fromNullable(picture.getDescription());
            Optional<String> technique = Optional.fromNullable(picture.getMedium());

            String painterDescriptionInfo = painterDescription.orNull();
            String pictureDescriptionInfo = pictureDescription.orNull();
            String techniqueInfo = technique.or("");
            String yearInfo = picture.getYear() > 0 ? String.valueOf(picture.getYear()) : "";
            String sizesInfo = (picture.getRealW() > 0 && picture.getRealH() > 0) ? String.valueOf(picture.getRealW()) + "x" + String.valueOf(picture.getRealH()) : "";

            //descriptionView.setText(Html.fromHtml(descriptionInfo));
            showHtmlIfNotEmptyOrGone(pictureDescriptionView, pictureDescriptionInfo);
            showHtmlIfNotEmptyOrGone(painterDescriptionView, painterDescriptionInfo);

            techniqueView.setText(techniqueInfo);
            yearView.setText(yearInfo);
            sizesView.setText(sizesInfo);

            site(painterSiteView, painter.getSite());
            facebook(painterFacebookView, painter.getFacebook());

            if(poparada){
                painterDescriptionView.setVisibility(View.GONE);
                sizesView.setVisibility(View.GONE);
            }

        }
        else {
            Timber.e("Picture is null -" + (picture == null) + "\n" + "Painter is null - " + (painter == null));
        }
        animatorView.setDisplayedChildId(R.id.picture_detail_content);

    }

    private static void showHtmlIfNotEmptyOrGone(TextView view, String html){
        if(!StringUtils.isEmpty(html)) {
            view.setVisibility(View.VISIBLE);
            view.setText(Html.fromHtml(html));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private static void showIfNotEmptyOrGone(TextView view, String info){
        if(!StringUtils.isEmpty(info)) {
            view.setVisibility(View.VISIBLE);
            view.setText(info);
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private void site(View view, String site){
        if(!StringUtils.isEmpty(site) && urlValidator.isValid(site)){
            view.setVisibility(View.VISIBLE);
            view.setOnClickListener(v -> Device.startBrowserLink(getContext(), site));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    private void facebook(View view, String facebook){
        if(!StringUtils.isEmpty(facebook) && urlValidator.isValid(facebook)){
            view.setVisibility(View.VISIBLE);
            view.setOnClickListener(v ->
                    AnimationHelper.pulse(v, () ->
                        Device.openFacebookPage(getContext(), facebook)
                    ));
        }
        else{
            view.setVisibility(View.GONE);
        }
    }

    @Override protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(!needToLoad){
            fill(this.picture, this.painter);
        }
        else{
            load();
        }
    }

    @Override protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        subscriptions.unsubscribe();
    }

    @OnClick(R.id.action_picture_detail_share) void share(View v){
        AnimationHelper.pulse(v, () -> {
            ShareHelper.sharePicture(getContext(), picture, urlValidator);
        });
    }

    @OnClick(R.id.picture_detail_image) void showPicture(View v){
        eventBus.post(new ShowPictureImageEvent(picture));
    }

    @OnClick(R.id.picture_detail_painter_image) void showPainter(View v){
        eventBus.post(new ShowPainterImageEvent(painter));
    }
}
