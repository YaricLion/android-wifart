package com.itdoors.whatifart.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import timber.log.Timber;

public final class AnimationHelper {

    private AnimationHelper(){
    }

    public static void showFab(View actionView, Runnable startAction){

        actionView.animate().cancel();
        ViewPropertyAnimator animator = actionView.animate().scaleX(1).scaleY(1).setDuration(200);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            animator.setListener( new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    startAction.run();
                }
            });
        }
        else{
            animator.withStartAction(startAction);
        }
        animator.start();
    }

    public static void hideFab(View actionView, Runnable endAction){

        actionView.animate().cancel();
        ViewPropertyAnimator animator = actionView.animate().scaleX(0).scaleY(0).setDuration(200);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            animator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    endAction.run();
                }
            });
        } else {
            animator.withEndAction(endAction);
        }
        animator.start();
    }

    public static void zoomInOutRotate(View target, Runnable endAction){

        AnimatorSet mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(
                ObjectAnimator.ofFloat(target, "scaleX", 1, 0.3f, 0, 0.3f, 1),
                ObjectAnimator.ofFloat(target, "scaleY", 1, 0.3f, 0, 0.3f, 1),
                ObjectAnimator.ofFloat(target, "rotation", 0, 0, -180, -120, 0),
                ObjectAnimator.ofFloat(target, "alpha", 1, 0.5f, 0, 0.5f, 1)
        );
        mAnimatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        mAnimatorSet.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                endAction.run();
            }
        });
        mAnimatorSet.setDuration(500);
        mAnimatorSet.start();

    }

    public static void pulse(View target, Runnable endAction){

        AnimatorSet mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(
                ObjectAnimator.ofFloat(target, "scaleY", 1, 1.5f, 1),
                ObjectAnimator.ofFloat(target, "scaleX", 1, 1.5f, 1)
        );
        mAnimatorSet.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {

                Timber.d("arFabAnimate endAction ");
                endAction.run();

            }
        });
        mAnimatorSet.setDuration(400);
        mAnimatorSet.start();

    }


}
