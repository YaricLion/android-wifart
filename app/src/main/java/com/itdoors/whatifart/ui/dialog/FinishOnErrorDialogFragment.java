package com.itdoors.whatifart.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.whatifart.R;

public class FinishOnErrorDialogFragment extends MessageDialogFragment{

    public static MessageDialogFragment newInstance(String title, String msg){
        MessageDialogFragment fragment = new FinishOnErrorDialogFragment();
        fragment.setArguments(getArgs(title, msg));
        return fragment;
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(getTitle())
                .content(getMessage())
                .positiveText(R.string.ar_ok)
                .build();

        View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        positiveAction.setOnClickListener(v -> {
            if (getActivity() != null) {
                getActivity().finish();
            }
            dialog.dismiss();
        });

        return dialog;
    }

    @Override public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

}
