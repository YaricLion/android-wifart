package com.itdoors.whatifart.data.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yariclion on 26.11.2015.
 */
public final class PainterResponse implements Parcelable {

    private final PainterPicturesInfo info;

    private final Bitmap painterImage;
    private final Bitmap pictureImage;

    public PainterResponse(PainterPicturesInfo info, Bitmap painterImage, Bitmap pictureImage) {
        this.info = info;
        this.painterImage = painterImage;
        this.pictureImage = pictureImage;
    }

    public Bitmap getPainterImage() {
        return painterImage;
    }

    public Bitmap getPictureImage() {
        return pictureImage;
    }

    public PainterPicturesInfo getInfo() {
        return info;
    }

    @Override  public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PainterResponse that = (PainterResponse) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(info, that.info)
                .append(painterImage, that.painterImage)
                .append(pictureImage, that.pictureImage)
                .isEquals();
    }

    @Override public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(info)
                .append(painterImage)
                .append(pictureImage)
                .toHashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.info, 0);
        dest.writeParcelable(this.painterImage, 0);
        dest.writeParcelable(this.pictureImage, 0);
    }

    protected PainterResponse(Parcel in) {
        this.info = in.readParcelable(PainterPicturesInfo.class.getClassLoader());
        this.painterImage = in.readParcelable(Bitmap.class.getClassLoader());
        this.pictureImage = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Parcelable.Creator<PainterResponse> CREATOR = new Parcelable.Creator<PainterResponse>() {
        public PainterResponse createFromParcel(Parcel source) {
            return new PainterResponse(source);
        }

        public PainterResponse[] newArray(int size) {
            return new PainterResponse[size];
        }
    };
}
