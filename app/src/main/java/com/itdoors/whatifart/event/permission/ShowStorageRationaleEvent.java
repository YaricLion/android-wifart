package com.itdoors.whatifart.event.permission;

public final class ShowStorageRationaleEvent {

    private final int code;

    public ShowStorageRationaleEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
