package com.itdoors.whatifart.data.api;

import android.app.Application;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false,
        library = true
)
public final class ApiModule {

        @Provides @Singleton PainterImageService provideTVMService(Picasso picasso, PainterService painterService) {
                return new PainterImageServiceImpl(picasso, painterService);
        }

        @Provides @Singleton PainterService providePainterService(Application app, Gson gson){
                return new PainterMockService(app, gson);
        }

}
