package com.itdoors.whatifart.event;

import android.graphics.Bitmap;

/**
 * Created by yariclion on 08.03.2016.
 */
public final class ScreenShotReadyEvent {
    private final Bitmap bitmap;

    public ScreenShotReadyEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
