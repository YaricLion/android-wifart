package com.itdoors.whatifart.ar;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;

import com.itdoors.whatifart.ui.gl.Texture;
import com.itdoors.whatifart.data.model.PainterResponse;

import rx.Observable;

public interface TextureService {
    Observable<Pair<Texture, Bitmap>> createTexture(PainterResponse painterResponce);
}
