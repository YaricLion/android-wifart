package com.itdoors.whatifart.analytics.event;

import java.util.Map;

public final class Event {

    private final String name;
    private final Map<String, String> params;

    public Event(String name, Map<String, String> params) {
        this.name = name;
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getParams() {
        return params;
    }

}
