package com.itdoors.whatifart;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.itdoors.whatifart.util.Device;
import com.itdoors.whatifart.util.Injector;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.LruCache;

import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;

import dagger.ObjectGraph;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends MultiDexApplication {

    private ObjectGraph objectGraph;
    private RefWatcher refWatcher;

    @Inject LruCache picassoMemCache;
    @Inject Device device;

    @Override public void onCreate() {
        super.onCreate();

        refWatcher = LeakCanary.install(this);
        buildObjectGraphAndInject();

        if (BuildConfig.DEBUG) {

            Timber.plant(new Timber.DebugTree());

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }
        else {

            if(BuildConfig.USE_FLURRY_ANALYTICS){
                configureAndInitFlurryAnalytics();
            }

            if (BuildConfig.USE_CRASHLYTICS) {
                Fabric.with(this, new Crashlytics());
                Timber.plant(new com.itdoors.whatifart.crashlytics.CrashReportingTree());
            }
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

    }

    private void configureAndInitFlurryAnalytics(){

        FlurryAgent.setLogEnabled(false);
        FlurryAgent.setCaptureUncaughtExceptions(true); ///????? TODO How flurry work with crashlytics
        FlurryAgent.setLogEvents(false);
        FlurryAgent.setVersionName(device.getVersion());
        FlurryAgent.setReportLocation(false);
        FlurryAgent.init(this, BuildConfig.FLURRY_APP_KEY);

    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }


    public static RefWatcher getRefWatcher(Context context) {
        App application = (App) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override public void onLowMemory() {
        clearPicassoMemCache();
        super.onLowMemory();
    }

    @Override public void onTrimMemory(int level) {
        if(level >= ComponentCallbacks2.TRIM_MEMORY_MODERATE){
            clearPicassoMemCache();
        }
        super.onTrimMemory(level);
    }

    private void clearPicassoMemCache(){
        if(picassoMemCache != null)
            picassoMemCache.evictAll();
    }

    public void buildObjectGraphAndInject() {
        objectGraph = ObjectGraph.create(Modules.list(this));
        objectGraph.inject(this);
    }

    @Override public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return objectGraph;
        }
        return super.getSystemService(name);
    }

}
