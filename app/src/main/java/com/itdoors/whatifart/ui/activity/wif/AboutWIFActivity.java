package com.itdoors.whatifart.ui.activity.wif;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.itdoors.whatifart.BuildConfig;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.ui.activity.YoutubeActivity;
import com.itdoors.whatifart.util.Device;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutWIFActivity extends YoutubeActivity implements YouTubePlayer.OnFullscreenListener{

    private final String WIF_VIDEO_LIST_ID = "PLcPxjzyl54whROn1Cy5nzDSlalWgjwuy_";

    @Bind(R.id.view_about_youtube)  YouTubePlayerView youTubeView;

    private YouTubePlayer player;
    private boolean fullscreen = false;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_wif);
        ButterKnife.bind(this);
        youTubeView.initialize(BuildConfig.YOUTUBE_DEVELOPER_API_KEY, this);
    }

    @Override protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;
        player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        player.setOnFullscreenListener(this);
        if (!wasRestored) {
            player.cuePlaylist(WIF_VIDEO_LIST_ID);
        }
    }

    @Override protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Nullable @OnClick(R.id.about_app_site_link) public void onSiteClicked(View v){
        String url = getResources().getString(R.string.app_site_link);
        Device.startBrowserLink(this, url);
    }

    @Override public void onBackPressed() {
        if(player != null && fullscreen)
            player.setFullscreen(false);
        else
            super.onBackPressed();
    }

    @Override public void onFullscreen(boolean isFullscreen) {
        fullscreen = isFullscreen;
    }

}
