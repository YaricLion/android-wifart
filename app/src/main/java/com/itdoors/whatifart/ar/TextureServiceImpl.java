package com.itdoors.whatifart.ar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Address;
import com.itdoors.whatifart.ui.gl.Texture;
import com.itdoors.whatifart.data.model.PainterPicturesInfo;
import com.itdoors.whatifart.data.model.PainterResponse;
import com.itdoors.whatifart.util.CircleStrokeTransformation;
import com.itdoors.whatifart.util.Device;
import com.google.common.base.Optional;
import com.itdoors.whatifart.util.TextureUtils;
import com.itdoors.whatifart.util.Transformation;

import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class TextureServiceImpl implements TextureService{

    private static final long DELAY_MILLIS = 1000L;

    private final Context context;
    private static volatile TextureServiceImpl singleton = null;

    public static TextureServiceImpl getInstance(Context context){
        if (singleton == null) {
            synchronized (TextureServiceImpl.class) {
                if (singleton == null) {
                    singleton = new TextureServiceImpl(context);
                }
            }
        }
        return singleton;
    }

    public TextureServiceImpl(Context context) {
        this.context = context;
    }

    @Override public Observable<Pair<Texture, Bitmap>> createTexture(PainterResponse painterResponse) {

        Observable<Pair<Texture, Bitmap>> pairObservable = Observable.create(observer -> {
            try {
                if (!observer.isUnsubscribed()) {
                    Pair<Texture, Bitmap> texture = createPainterTextureBitmap(context, painterResponse);
                    observer.onNext(texture);
                    observer.onCompleted();
                }
            } catch (Exception e) {
                observer.onError(e);
            }
        });

        return Observable.defer(() -> pairObservable).delay(DELAY_MILLIS, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io());

    }

    private static Pair<Texture, Bitmap> createPainterTextureBitmap(Context context, PainterResponse painterResponse){

        Device.throwIfOnMainThread();

        try {

            int mTextureW = ARConfig.textureW, mTextureH = ARConfig.textureH;

            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout rootView = new RelativeLayout(context);
            mInflater.inflate(R.layout.view_ar, rootView, true);

            Typeface face = Typeface.createFromAsset(context.getAssets(),"fonts/Jura-Medium.ttf");

            Optional<ImageView> painterPictureView      = Optional.fromNullable((ImageView) rootView.findViewById(R.id.view_painter_logo));
            Optional<TextView>  painterNameView         = Optional.fromNullable((TextView) rootView.findViewById(R.id.view_painter_name));
            Optional<TextView>  painterSurnameView      = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_surname) );
            Optional<TextView>  painterCityView         = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_adress_city) );
            Optional<TextView>  painterAddressCommaView = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_adress_comma) );
            Optional<TextView>  painterCountryView      = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_adress_country) );
            Optional<TextView>  painterDescriptionView  = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_desrc) );

            Optional<ImageView> pictureView             = Optional.fromNullable ( (ImageView)rootView.findViewById(R.id.view_painter_picture) );
            Optional<TextView>  pictureNameView         = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_picture_name) );
            Optional<TextView>  pictureYearView         = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_picture_year) );
            Optional<TextView>  pictureYearCommaView    = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_picture_year_medium_comma) );
            Optional<TextView>  pictureMediumView       = Optional.fromNullable ( (TextView)rootView.findViewById(R.id.view_painter_picture_medium) );

            Optional<LinearLayout>  infoHolder          = Optional.fromNullable ( (LinearLayout)rootView.findViewById(R.id.view_painter_picture_info_holder) );

            PainterPicturesInfo info = painterResponse.getInfo();
            Bitmap painterImage = painterResponse.getPainterImage();
            Bitmap pictureImage = painterResponse.getPictureImage();

            if(painterNameView.isPresent()) {
                painterNameView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterPictureNameTextSize);
                painterNameView.get().setTypeface(face, Typeface.BOLD);
                painterNameView.get().setText(Optional.fromNullable(info.getPainter().getName()).or(""));
            }
            if(painterSurnameView.isPresent()) {
                painterSurnameView.get().setTypeface(face, Typeface.BOLD);
                painterSurnameView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterPictureNameTextSize);
                painterSurnameView.get().setText(Optional.fromNullable(info.getPainter().getSurname()).or(""));
            }
            if(painterDescriptionView.isPresent()) {
                painterDescriptionView.get().setTypeface(face);
                painterDescriptionView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterDesrcTextSize);
                painterDescriptionView.get().setText(Optional.fromNullable(info.getPainter().getDescription()).or(""));
            }

            Address stubAddress = new Address(null,null);
            Address address = Optional.fromNullable(info.getPainter().getAddress()).or(stubAddress);

            if(painterCityView.isPresent()) {
                painterCityView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterAdressTextSize);
                painterCityView.get().setText(Optional.fromNullable(address.getCity()).or(""));
            }
            if(painterAddressCommaView.isPresent()) {
                painterAddressCommaView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterAdressTextSize);
                if(StringUtils.isEmpty(address.getCity()))
                    painterAddressCommaView.get().setVisibility(View.INVISIBLE);
            }
            if(painterCountryView.isPresent()) {
                painterCountryView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.painterAdressTextSize);
                painterCountryView.get().setText(Optional.fromNullable(address.getCountry()).or(""));
            }

            if(pictureNameView.isPresent()) {
                pictureNameView.get().setTypeface(face, Typeface.BOLD);
                pictureNameView.get().setTextSize(TypedValue.COMPLEX_UNIT_PX, ARConfig.pictureNameTextSize);
                pictureNameView.get().setText(Optional.fromNullable(info.getPicture().getName()).or(context.getString(R.string.without_name)));
            }

            if(pictureYearView.isPresent()) {
                pictureYearView.get().setTypeface(face);
                int year = info.getPicture().getYear();
                String yearInfo = year > 0 ? Integer.toString(year) : "";
                pictureYearView.get().setText(yearInfo);
            }

            if(pictureYearCommaView.isPresent()) {
                pictureYearCommaView.get().setTypeface(face);
                int year = info.getPicture().getYear();
                if(year <= 0){
                    pictureYearCommaView.get().setVisibility(View.INVISIBLE);
                }

            }
            if(pictureMediumView.isPresent()) {
                pictureMediumView.get().setTypeface(face);
                pictureMediumView.get().setText(Optional.fromNullable(info.getPicture().getMedium()).or(""));
            }

            int realH = painterResponse.getInfo().getPicture().getRealH();
            int realW = painterResponse.getInfo().getPicture().getRealW();

            Sizes size = new Sizes(realW, realH);
            size = TextureUtils.resizeIfNeed(size);

            if(pictureView.isPresent()) {

                ViewGroup.LayoutParams lp = pictureView.get().getLayoutParams();
                lp.height = (int) size.getH();
                lp.width  = (int) size.getW();

                ((RelativeLayout.LayoutParams )lp).setMargins(ARConfig.frameSize, ARConfig.frameSize, ARConfig.frameSize, ARConfig.frameSize);
                pictureView.get().setImageBitmap(pictureImage);

            }
            if(infoHolder.isPresent()) {

                ViewGroup.LayoutParams lp = infoHolder.get().getLayoutParams();
                ((LinearLayout.LayoutParams )lp).setMargins(ARConfig.painterPictureInfoHolderMargin, ARConfig.painterPictureInfoHolderMargin, ARConfig.painterPictureInfoHolderMargin, ARConfig.painterPictureInfoHolderMargin);
            }

            if(painterPictureView.isPresent()) {

                ViewGroup.LayoutParams lp = painterPictureView.get().getLayoutParams();
                lp.height = ARConfig.painterLogoSize;
                lp.width  = ARConfig.painterLogoSize;

                Transformation transformation = new CircleStrokeTransformation(context, Color.WHITE, 1);
                if(painterImage != null) {
                    painterPictureView.get().setImageBitmap(transformation.transform(painterImage));
                }else {
                    Bitmap stub = BitmapFactory.decodeResource(context.getResources(), R.drawable.contact_icon);
                    painterPictureView.get().setImageBitmap(transformation.transform(stub));
                }

            }

            rootView.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            rootView.measure(View.MeasureSpec.makeMeasureSpec(mTextureW, View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(mTextureH, View.MeasureSpec.EXACTLY));
            rootView.layout(0, 0, rootView.getMeasuredWidth(), rootView.getMeasuredHeight());

            Bitmap bitmap = Bitmap.createBitmap(mTextureW, mTextureH, Bitmap.Config.ARGB_8888);

            Canvas c = new Canvas(bitmap);
            rootView.draw(c);
            Texture texture = Texture.generateTexture(bitmap, size);
            return new Pair<>(texture, bitmap);

        }
        catch (RuntimeException exception){
            Timber.e(exception, "TextureService. Fail to create texture");
            throw exception;
        }
    }

}