package com.itdoors.whatifart.ui;

/**
 * Created by yariclion on 07.05.2015.
 */
public final class Intents {

    private Intents(){
        throw new AssertionError("No instances.");
    }

    public static final class Picture{
        public static final String PICTURE = "com.itdoors.wifart.intents.picture";
        public static final String PICTURE_ID = "com.itdoors.wifart.intents.picture_id";
        public static final String PICTURE_URL = "com.itdoors.wifart.intents.picture_url";
    }

    public static final class Painter{
        public static final String PAINTER = "com.itdoors.wifart.intents.painter";
        public static final String PAINTER_ID = "com.itdoors.wifart.intents.painter_id";
        public static final String PAINTER_URL = "com.itdoors.wifart.intents.painter_url";
    }

    public static final class Tutorial{
        public static final String FIRST_LAUNCH = "com.itdoors.wifart.intents.FIRST_LAUNCH";
    }

}
