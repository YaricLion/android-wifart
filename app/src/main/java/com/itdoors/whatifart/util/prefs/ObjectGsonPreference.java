package com.itdoors.whatifart.util.prefs;

import com.google.gson.Gson;

/**
 * Created by v014nd on 24.03.2016.
 */
public class ObjectGsonPreference<T> implements Preference<T>{

    private final StringPreference prefs;
    private final Gson gson;
    private final Class<T> clazz;

    public ObjectGsonPreference(StringPreference prefs, Gson gson, Class<T> clazz) {
        this.prefs = prefs;
        this.gson = gson;
        this.clazz = clazz;
    }

    public void set(T value) {
        prefs.set(gson.toJson(value));
    }

    public T get() {
        return gson.fromJson(prefs.get(), clazz);
    }

    public boolean isSet() {
        return prefs.isSet();
    }

    public void delete() {
        prefs.delete();
    }
}
