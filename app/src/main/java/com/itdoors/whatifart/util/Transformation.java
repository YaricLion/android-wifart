package com.itdoors.whatifart.util;

import android.graphics.Bitmap;

public interface Transformation {
	public Bitmap transform(Bitmap bitmap);
}