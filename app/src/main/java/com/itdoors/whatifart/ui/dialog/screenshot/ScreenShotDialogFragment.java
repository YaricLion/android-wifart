package com.itdoors.whatifart.ui.dialog.screenshot;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.itdoors.whatifart.App;
import com.itdoors.whatifart.R;
import com.itdoors.whatifart.event.permission.AllowStorageEvent;
import com.itdoors.whatifart.event.permission.DenyStorageEvent;
import com.itdoors.whatifart.event.permission.ShowStorageRationaleEvent;
import com.itdoors.whatifart.ui.ScreenShotService;
import com.itdoors.whatifart.data.model.Picture;
import com.itdoors.whatifart.ui.activity.main.MainARActivity;
import com.itdoors.whatifart.util.AnimationHelper;
import com.itdoors.whatifart.util.ToastUtils;
import com.itdoors.whatifart.util.ViewSubscriptions;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.ObjectGraph;
import de.greenrobot.event.EventBus;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ScreenShotDialogFragment extends DialogFragment {

    private static final int PERMISSIONS_REQUEST_STORAGE = 1;

    public static final int ACTION_NONE  = -1;
    public static final int ACTION_SAVE  = 0;
    public static final int ACTION_SHARE = 1;

    private static final String BM_TAG = "bm";
    private static final String PIC_TAG = "pic";

    @Bind(R.id.screen_shot_share) View shareView;
    @Bind(R.id.screen_shot_save)  View saveView;
    @Bind(R.id.screenShotImgView) ImageView screenShot;

    @Inject ScreenShotService screenShotService ;
    @Inject EventBus eventBus;

    private final ViewSubscriptions subscriptions = new ViewSubscriptions(Schedulers.io(), AndroidSchedulers.mainThread() );

    private int requested_action = ACTION_NONE;

    public static ScreenShotDialogFragment newInstance(Bitmap bm, Picture picture){

        Bundle bundle = new Bundle();
        bundle.putParcelable(BM_TAG, bm);
        bundle.putParcelable(PIC_TAG, picture);
        ScreenShotDialogFragment fragment = new ScreenShotDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override public void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }


    public void onEventMainThread(AllowStorageEvent event){
        requestStorage();
    }

    public void onEventMainThread(DenyStorageEvent event){
        failEnd();
    }

    private void failEnd(){

        if(requested_action == ACTION_SAVE) {
            ToastUtils.ToastLong(getActivity().getApplicationContext(), getString(R.string.permission_storage_screenshot_rationale_save_dialog_denied_msg));
            requested_action = ACTION_NONE;
        }
        else if(requested_action == ACTION_SHARE) {
            ToastUtils.ToastLong(getActivity().getApplicationContext(), getString(R.string.permission_storage_screenshot_rationale_share_dialog_denied_msg));
            requested_action = ACTION_NONE;
        }
        else {
            throw new IllegalStateException("Failed action code : " + requested_action);
        }
    }


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Override  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ObjectGraph objectGraph = ((MainARActivity)getActivity()).getObjectGraph();
        objectGraph.inject(this);

        View view = inflater.inflate(R.layout.fragment_dialog_screenshot, container, false);
        ButterKnife.bind(this, view);

        final Bitmap  bm  = getBitmap();
        final Picture pic = getPicture();

        shareView.setOnClickListener(v ->
            AnimationHelper.zoomInOutRotate(v, () -> {
                if(hasStoragePermission()) {
                    shareScreenShot(bm, pic);
                }else {
                    requested_action = ACTION_SHARE;
                    requestStorage();
                }
            })
        );

        saveView.setOnClickListener(v ->
                        AnimationHelper.zoomInOutRotate(v, () -> {
                            if (hasStoragePermission()) {
                                saveScreenShot(bm, pic);
                            } else {
                                requested_action = ACTION_SAVE;
                                requestStorage();
                            }
                        })
        );
        screenShot.setImageBitmap(bm);
        return view;

    }

    private Bitmap getBitmap(){
        return getArguments().getParcelable(BM_TAG);
    }
    private Picture getPicture(){
        return getArguments().getParcelable(PIC_TAG);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        subscriptions.unsubscribe();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        App.getRefWatcher(getActivity()).watch(this);
    }

    private void shareScreenShot(Bitmap bm, Picture picture){
        lock();
        subscriptions.add(screenShotService.share(bm, picture),
                intent -> {
                    startActivity(intent);
                    dismiss();
                },
                throwable -> {
                    ToastUtils.ToastLong(getActivity().getApplicationContext(), getString(R.string.ar_share_pic_failed));
                    unlock();
                });

    }

    private void saveScreenShot(Bitmap bm, Picture picture){
        lock();
        subscriptions.add(screenShotService.save(bm, picture),
                file -> {
                    Timber.d("Picture saved to " + file.getAbsolutePath());
                    ToastUtils.ToastLong(getActivity().getApplicationContext(), "Picture saved to " + file.getAbsolutePath());//getString(R.string.ar_save_pic_ok));
                    dismiss();
                },
                throwable -> {
                    ToastUtils.ToastLong(getActivity().getApplicationContext(), getString(R.string.ar_save_pic_failed));
                    unlock();
                });
    }

    private void lock(){
        saveView.setEnabled(false);
        shareView.setEnabled(false);
    }

    private void unlock(){
        saveView.setEnabled(true);
        shareView.setEnabled(true);
    }

    @Override  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                finishRequestedAction();
            } else {
                // if permission was denied check if we should ask again in the future (i.e. they
                // did not check 'never ask again')
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        || shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    eventBus.post(new ShowStorageRationaleEvent(requested_action));
                } else {
                    // denied & shouldn't ask again. deal with it (•_•) ( •_•)>⌐■-■ (⌐■_■)
                    failEnd();
                }
            }
        }
    }

    private void finishRequestedAction(){

        final Bitmap  bm  = getBitmap();
        final Picture pic = getPicture();

        switch (requested_action){
            case ACTION_SAVE:
                saveScreenShot(bm, pic);
                break;
            case ACTION_SHARE:
                shareScreenShot(bm, pic);
                break;
            default:
                throw new IllegalStateException("Failed action : " + requested_action);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestStorage(){
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_STORAGE);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean hasStoragePermission(){
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


}



