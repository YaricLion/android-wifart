package com.itdoors.whatifart.data.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class Painter implements Parcelable {

    @SerializedName("id")
    private final String id;
    @SerializedName("name")
    private final String name;
    @SerializedName("surname")
    private final String surname;
    @SerializedName("description")
    private final String description;
    @SerializedName("imageLink")
    private final String imageLink;
    @SerializedName("address")
    private final Address address;
    @SerializedName("facebook")
    private final String facebook;
    @SerializedName("site")
    private final String site;


    public Painter(String id, String name, String surname, String description, String imageLink, Address address, String facebook, String site) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.description = description;
        this.imageLink = imageLink;
        this.address = address;
        this.facebook = facebook;
        this.site = site;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public Address getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getSite() {
        return site;
    }

    public String getSurname() {
        return surname;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Painter painter = (Painter) o;

        return new EqualsBuilder()
                .append(id, painter.id)
                .append(name, painter.name)
                .append(surname, painter.surname)
                .append(description, painter.description)
                .append(imageLink, painter.imageLink)
                .append(address, painter.address)
                .append(facebook, painter.facebook)
                .append(site, painter.site)
                .isEquals();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(surname)
                .append(description)
                .append(imageLink)
                .append(address)
                .append(facebook)
                .append(site)
                .toHashCode();
    }

    @Override public String toString() {
        return "Painter{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", description='" + description + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", address=" + address +
                ", facebook='" + facebook + '\'' +
                ", site='" + site + '\'' +
                '}';
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.description);
        dest.writeString(this.imageLink);
        dest.writeParcelable(this.address, 0);
        dest.writeString(this.facebook);
        dest.writeString(this.site);
    }

    protected Painter(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.surname = in.readString();
        this.description = in.readString();
        this.imageLink = in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.facebook = in.readString();
        this.site = in.readString();
    }

    public static final Creator<Painter> CREATOR = new Creator<Painter>() {
        public Painter createFromParcel(Parcel source) {
            return new Painter(source);
        }

        public Painter[] newArray(int size) {
            return new Painter[size];
        }
    };
}
