package com.itdoors.whatifart.crashlytics;

import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.common.base.Optional;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

public final class FailReport {

    private FailReport() {
        throw new AssertionError("No instances.");
    }

    public static void logError(Throwable t, String tag, @Nullable String message) {
       log(getReport(tag, message), t);
    }

    public static void logLoggingError(Throwable logging, Throwable t, @Nullable String message, @Nullable Object... args){
        String report = getReport(null, message, args);
        log(getLoggingReport(report, logging), t);
    }

    private static void log(String report, Throwable t){
        CrashlyticsCore core = Crashlytics.getInstance().core;
        core.log(Optional.fromNullable(report).or("[?report?]"));
        if(t != null)
            core.logException(t);
    }

    private static String getLoggingReport(String report, Throwable logging){
        return "[ logging report ] : "  + report + "\n" + " [ exception ] : " + getStackTraceString(logging);
    }

    private static String getReport(String tag, String message, Object... args){

        String msg = null;
        if(args != null && args.length > 0) {
            try {
                msg = String.format(message, args);
            }catch (RuntimeException stupid){
                msg = "[ message ] : " + message + " [ with args ] : " + Arrays.toString(args) + " \n[ throws exception ] : \n " + getStackTraceString(stupid);
            }
        }
        return "[ tag ] : " + Optional.fromNullable(tag).or("[?tag?]") + "\n" +
               "[ message ]  : " +
                   Optional.fromNullable(msg).or(
                       Optional.fromNullable(message).or("[?mg?]")
                   );

    }

    public static String getStackTraceString(Throwable t) {
        StringWriter sw = new StringWriter(256);
        PrintWriter pw = new PrintWriter(sw, false);
        t.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

}
