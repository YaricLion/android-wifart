package com.itdoors.whatifart.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class PainterPicturesInfo implements Parcelable {


    @SerializedName("painter")
    private final Painter painter;
    @SerializedName("picture")
    private final Picture picture;

    public PainterPicturesInfo(Painter painter, Picture picture) {
        this.painter = painter;
        this.picture = picture;
    }

    public Painter getPainter() {
        return painter;
    }

    public Picture getPicture() {
        return picture;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PainterPicturesInfo that = (PainterPicturesInfo) o;

        return new EqualsBuilder()
                .append(painter, that.painter)
                .append(picture, that.picture)
                .isEquals();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(painter)
                .append(picture)
                .toHashCode();
    }

    @Override public String toString() {
        return "PainterPicturesInfo{" +
                "painter=" + painter +
                ", picture=" + picture +
                '}';
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.painter, 0);
        dest.writeParcelable(this.picture, 0);
    }

    protected PainterPicturesInfo(Parcel in) {
        this.painter = in.readParcelable(Painter.class.getClassLoader());
        this.picture = in.readParcelable(Picture.class.getClassLoader());
    }

    public static final Creator<PainterPicturesInfo> CREATOR = new Creator<PainterPicturesInfo>() {
        public PainterPicturesInfo createFromParcel(Parcel source) {
            return new PainterPicturesInfo(source);
        }

        public PainterPicturesInfo[] newArray(int size) {
            return new PainterPicturesInfo[size];
        }
    };
}
