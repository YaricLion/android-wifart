package com.itdoors.whatifart.data.api;

import com.itdoors.whatifart.data.model.PainterPicturesInfo;

import rx.Observable;

public interface PainterService {
    Observable<PainterPicturesInfo> getPainterInfo(String uid);
}
