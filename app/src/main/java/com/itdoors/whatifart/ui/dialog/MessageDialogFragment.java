package com.itdoors.whatifart.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.itdoors.whatifart.R;

public class MessageDialogFragment extends DialogFragment {

    public static MessageDialogFragment newInstance(String title, String msg){
        MessageDialogFragment fragment = new MessageDialogFragment();
        fragment.setArguments(getArgs(title, msg));
        return fragment;
    }

    protected static Bundle getArgs(String title, String msg) {

        Bundle args = new Bundle();
        args.putString("args_msg", msg);
        args.putString("args_title", title);
        return args;
    }

    public String getMessage(){
        return getParams("args_msg");
    }

    public String getTitle(){
        return getParams("args_title");
    }

    private String getParams(String key){
        if(getArguments() != null){
            return getArguments().getString(key);
        }
        return null;
    }

    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(getTitle())
                .content(getMessage())
                .positiveText(R.string.ar_ok)
                .build();

        return dialog;

    }

}
