package com.itdoors.whatifart.ar;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by v014nd on 17.03.2016.
 */
public final class Sizes {

    private final float w;
    private final float h;

    public Sizes(float w, float h) {
        this.w = w;
        this.h = h;
    }

    public float getH() {
        return h;
    }

    public float getW() {
        return w;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sizes that = (Sizes) o;
        return new EqualsBuilder()
                .append(w, that.w)
                .append(h, that.h)
                .isEquals();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(w)
                .append(h)
                .toHashCode();
    }
}
