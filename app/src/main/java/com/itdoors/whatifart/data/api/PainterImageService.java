package com.itdoors.whatifart.data.api;

import com.itdoors.whatifart.data.model.PainterResponse;

import rx.Observable;

public interface PainterImageService {
    Observable<PainterResponse> getPainterPicturesInfo(String uid);
}
