package com.itdoors.whatifart;

import android.app.Application;

import com.itdoors.whatifart.ar.ARModule;
import com.itdoors.whatifart.data.DataModule;
import com.itdoors.whatifart.ui.UiModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        includes = {
                UiModule.class,
                DataModule.class,
                ARModule.class
        },
        injects = {
                App.class
        }
)
public final class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @Singleton Application provideApplication() {
        return app;
    }

}
