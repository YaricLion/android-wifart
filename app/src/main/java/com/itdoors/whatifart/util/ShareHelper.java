package com.itdoors.whatifart.util;

import android.content.Context;
import android.content.Intent;

import com.itdoors.whatifart.R;
import com.itdoors.whatifart.data.model.Picture;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;

public final class ShareHelper {

    private ShareHelper(){
        throw new AssertionError("No instances.");
    }

    public static final Intent getShareLinkIntent(String url, String msg){

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, msg);
        share.putExtra(Intent.EXTRA_TEXT, url);
        return share;

    }

    public static final Intent getShareLinkIntent(String url){

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, url);
        return share;

    }

    public static final void sharePicture(Context context, Picture picture, UrlValidator urlValidator){

        String shareDialogTitle = context.getString(R.string.share_link);
        String picName = picture.getName(), picUrl = picture.getShareLink();

        Intent intent;
        if(!StringUtils.isEmpty(picUrl) && urlValidator.isValid(picUrl)) {
            if(StringUtils.isEmpty(picName))
                picName = context.getString(R.string.picture_this);
            String msg = String.format(context.getString(R.string.share_picture_link_text), picName);
            intent = getShareLinkIntent(picUrl, msg);
        }
        else {
            String wifartSite = context.getString(R.string.wifart_site_link);
            intent = getShareLinkIntent(wifartSite);
        }
        context.startActivity(Intent.createChooser(intent, shareDialogTitle));

    }

}
