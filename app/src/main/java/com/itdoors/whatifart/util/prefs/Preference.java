package com.itdoors.whatifart.util.prefs;

public interface Preference<T> {
    T get();
    boolean isSet();
    void set(T value);
    void delete();
}
