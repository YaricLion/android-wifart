package com.itdoors.whatifart.util;

import android.content.Context;
import java.lang.AssertionError;import java.lang.String;import java.lang.SuppressWarnings;
import dagger.ObjectGraph;

public final class Injector {
    private static final String INJECTOR_SERVICE = "com.jakewharton.u2020.injector";

    @SuppressWarnings({"ResourceType", "WrongConstant"}) // Explicitly doing a custom service.
    public static ObjectGraph obtain(Context context) {
        return (ObjectGraph) context.getSystemService(INJECTOR_SERVICE);
    }

    public static boolean matchesService(String name) {
        return INJECTOR_SERVICE.equals(name);
    }

    private Injector() {
        throw new AssertionError("No instances.");
    }
}